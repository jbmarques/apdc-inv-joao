# Source code folder for the various projects

The final ADT implementation is in the [adt folder](adt/) along with a collection of unit tests for the implemented operations.
The project uses [maven](http://maven.apache.org/) to manage its dependencies and build process.
To run the unit tests [PostgreSQL](https://www.postgresql.org/) version 10 must be installed and a database called `testDB` must
exist. The tests assume that the system user that runs the tests has read and write access to the test database, if this is not
the case, please edit the `USER` and `PASS` constants in the [DatabaseTest.java](adt/src/test/java/DatabaseTest.java) file.
