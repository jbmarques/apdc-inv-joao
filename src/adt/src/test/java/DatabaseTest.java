import apdc.db.*;
import apdc.db.condition.*;
import apdc.db.exception.*;

import org.junit.Assert;
import org.junit.*;

import java.sql.*;
import java.util.Properties;

public class DatabaseTest {
    private static final String URL  = "jdbc:postgresql://localhost:5432/testDB";
    private static final String USER = null;
    private static final String PASS = null;

    private void setup() throws Exception {
        Properties props = new Properties();
        if (USER != null) props.setProperty("user", USER);
        if (PASS != null) props.setProperty("password", PASS);

        Connection conn = DriverManager.getConnection(URL, props);
        DatabaseMetaData md = conn.getMetaData();

        // Drop all user defined tables.
        ResultSet rs = md.getTables(null, null, "%", new String[] {"TABLE"});
        while (rs.next()) {
            String query = String.format("DROP TABLE IF EXISTS \"%s\" CASCADE;", rs.getString(3));
            Statement st = conn.createStatement();
            st.executeUpdate(query);
            st.close();
        }
        conn.close();
    }

    @Test
    public void createTableTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column product_no = new Column("product_no", Types.INTEGER);
            Column[] columns = new Column[] { product_no };

            Table products = new Table("products", columns);
            products.setPrimaryKey(columns);

            database.createTable(products);

            Assert.assertNotNull(database.getTable("products"));

            database.close();
            database = new Database(URL, USER, PASS,false, System.out);
            Assert.assertNotNull(database.getTable("products"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void dropTableTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column order_id = new Column("order_id", Types.INTEGER);
            Column quantity = new Column("quantity", Types.INTEGER);

            Table orders = new Table("orders", new Column[] { order_id, quantity });
            orders.setPrimaryKey(new Column[] { order_id });

            database.createTable(orders);

            Assert.assertNotNull(database.getTable("orders"));
            database.dropTable(orders);

            database.close();
            database = new Database(URL, USER, PASS, false, System.out);
            Assert.assertNull(database.getTable("orders"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void addColumnTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column order_id = new Column("order_id", Types.INTEGER);
            Column quantity = new Column("quantity", Types.INTEGER);

            // Create table without quantity column.
            Table orders = new Table("orders", new Column[]{ order_id });
            orders.setPrimaryKey(new Column[] { order_id });

            database.createTable(orders);

            Assert.assertNotNull(database.getTable("orders"));
            Assert.assertNotNull(orders.getColumn("order_id"));

            orders.addColumn(quantity);
            Assert.assertNotNull(orders.getColumn("quantity"));

            database.close();
            database = new Database(URL, USER, PASS, false, System.out);
            Assert.assertNotNull(database.getTable("orders").getColumn("quantity"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void removeColumnTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column order_id = new Column("order_id", Types.INTEGER);
            Column quantity = new Column("quantity", Types.INTEGER);

            Table orders = new Table("orders", new Column[] {order_id, quantity});
            orders.setPrimaryKey(new Column[] { order_id });

            database.createTable(orders);

            Assert.assertNotNull(database.getTable("orders"));
            Assert.assertNotNull(orders.getColumn("order_id"));
            Assert.assertNotNull(orders.getColumn("quantity"));

            orders.dropColumn("quantity");
            Assert.assertNull(orders.getColumn("quantity"));

            database.close();
            database = new Database(URL, USER, PASS, false, System.out);
            Assert.assertNotNull(database.getTable("orders").getColumn("order_id"));
            Assert.assertNull(database.getTable("orders").getColumn("quantity"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void renameTableTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column product_no = new Column("product_no", Types.INTEGER);
            Column[] columns = new Column[] { product_no };

            Table products = new Table("products", columns);
            products.setPrimaryKey(columns);

            database.createTable(products);

            Assert.assertNotNull(database.getTable("products"));

            products.rename("products_copy");

            Assert.assertNull(database.getTable("products"));
            Assert.assertNotNull(database.getTable("products_copy"));

            database.close();
            database = new Database(URL, USER, PASS, false, System.out);
            Assert.assertNull(database.getTable("products"));
            Assert.assertNotNull(database.getTable("products_copy"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void renameColumnTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column order_id = new Column("order_id", Types.INTEGER);

            Table orders = new Table("orders", new Column[] { order_id });
            orders.setPrimaryKey(new Column[] { order_id });

            database.createTable(orders);

            Assert.assertNotNull(orders.getColumn("order_id"));

            order_id.rename("order_id_copy");

            Assert.assertNull(orders.getColumn("order_id"));
            Assert.assertNotNull(orders.getColumn("order_id_copy"));

            database.close();
            database = new Database(URL, USER, PASS, false, System.out);
            Assert.assertNull(database.getTable("orders").getColumn("order_id"));
            Assert.assertNotNull(database.getTable("orders").getColumn("order_id_copy"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void referentialIntegritySimpleTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column email = new Column("email", Types.VARCHAR, 50);

            Table t1 = new Table("t1", new Column[] {email});
            t1.setPrimaryKey(new Column[] { email });

            Column email_fk = new Column("email", Types.VARCHAR, 50);
            email_fk.setForeignKeyReference(email);

            Table t2 = new Table("t2", new Column[] {email_fk});
            t2.setPrimaryKey(new Column[] { email_fk});

            database.createTable(t1);
            database.createTable(t2);

            Assert.assertNotNull(t1.getColumn("email"));
            Assert.assertNotNull(t2.getColumn("email"));
            try {
                database.dropTable(t1);
            } catch (ReferentialIntegrityException e) {
                // All good, drop table shouldn't work.
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.fail();
    }

    @Test
    public void referentialIntegrityRenameTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column email = new Column("email", Types.VARCHAR, 50);

            Table t1 = new Table("t1", new Column[] {email});
            t1.setPrimaryKey(new Column[] { email });

            Column email_fk = new Column("email", Types.VARCHAR, 50);
            email_fk.setForeignKeyReference(email);

            Table t2 = new Table("t2", new Column[] {email_fk});
            t2.setPrimaryKey(new Column[] { email_fk });

            database.createTable(t1);
            database.createTable(t2);

            email.rename("emailColumn");

            Assert.assertNull(t1.getColumn("email"));
            Assert.assertNotNull(t1.getColumn("emailColumn"));
            Assert.assertNotNull(t2.getColumn("email"));

            try {
                database.dropTable(t1);
            } catch (ReferentialIntegrityException e) {
                // All good, drop table shouldn't work.
                Assert.assertTrue(email_fk.getForeignKeyReference().getName().equals("emailColumn"));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        Assert.fail();
    }

    @Test
    public void conditionTest() {
        try {
            Column c1 = new Column("c1", Types.INTEGER);
            Column c2 = new Column("c2", Types.INTEGER);
            Column c3 = new Column("c3", Types.INTEGER);
            Constant<Integer> num = new Constant<>(100);

            Condition equal = new Equal(c1, c2);
            Condition lte = new LessThanOrEqual(c3, num);

            Condition cond = new And(equal, lte);
            Assert.assertTrue(cond.toSQL().equals("((c1 = c2) AND (c3 <= 100))"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void insertSelectTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            int numInserts = 10;
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column product_no = new Column("product_no", Types.INTEGER);
            Column quantity = new Column("quantity", Types.INTEGER);

            Column[] columns = new Column[] { product_no, quantity };

            Table products = new Table("products", columns);
            products.setPrimaryKey(new Column[] { product_no });

            database.createTable(products);

            for (int i = 1; i <= numInserts; i++)
                products.insert(columns, new Object[] { i, i * 10 });

            Row[] rows = products.selectAll();
            Assert.assertEquals(rows.length, numInserts);

            for (Row r : rows) {
                Integer pno = (Integer) r.getValue(product_no.getName());
                Integer qty = (Integer) r.getValue(quantity.getName());
                Assert.assertTrue(qty == pno * 10);
            }

            Condition cond = new LessThanOrEqual(product_no, new Constant<Integer>(numInserts / 2));
            Assert.assertEquals(numInserts / 2, products.select(cond).length);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void joinTablesTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column pk = new Column("pk", Types.INTEGER);
            Column c1 = new Column("c1", Types.INTEGER);
            Column[] t1Columns = new Column[] { pk, c1 };

            Table t1 = new Table("t1", t1Columns);
            t1.setPrimaryKey(new Column[] { pk });

            database.createTable(t1);

            Column fk = new Column("fk", Types.INTEGER);
            Column c2 = new Column("c2", Types.INTEGER);
            Column[] t2Columns = new Column[] { fk, c2 };

            Table t2 = new Table("t2", t2Columns);
            t2.setPrimaryKey(new Column[] { fk });
            fk.setForeignKeyReference(pk);

            database.createTable(t2);

            t1.insert(t1Columns, new Object[] { 1, 10 });
            t2.insert(t2Columns, new Object[] { 1, 20 });

            database.innerJoinTablesIntoTable("t3", t1, t2, new Equal(pk, fk));

            // Resulting table.
            Table t3 = database.getTable("t3");
            // Views of the original tables.
            View v1 = database.getView("t1");
            View v2 = database.getView("t2");

            Row[] rowsV1 = v1.selectAll();
            Assert.assertEquals(1, rowsV1.length);
            Row[] rowsV2 = v2.selectAll();
            Assert.assertEquals(1, rowsV2.length);
            Row[] rowsT3 = t3.selectAll();
            Assert.assertEquals(1, rowsT3.length);

            Assert.assertEquals(rowsV1[0].getValue("c1"), rowsT3[0].getValue("c1"));
            Assert.assertEquals(rowsV2[0].getValue("c2"), rowsT3[0].getValue("c2"));

            Assert.assertEquals(null, rowsV1[0].getValue("c2"));
            Assert.assertEquals(null, rowsV2[0].getValue("c1"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void splitTableTest() {
        try {
            setup();
        } catch (Exception e) {
            Assert.fail("PostgreSQL setup failed!");
        }

        try {
            Database database = new Database(URL, USER, PASS, false, System.out);

            Column pk1 = new Column("pk1", Types.INTEGER);
            Column pk2 = new Column("pk2", Types.INTEGER);
            Column pk3 = new Column("pk3", Types.INTEGER);

            Column c1 = new Column("c1", Types.INTEGER);
            Column c2 = new Column("c2", Types.INTEGER);

            Column[] columns = { pk1, pk2, pk3, c1, c2 };

            Table t1 = new Table("t1", columns);
            t1.setPrimaryKey(new Column[] {pk1, pk2, pk3});

            database.createTable(t1);
            t1.insert(columns, new Object[] { 1, 2, 3, 10, 20});

            database.splitTableIntoTables("t2", "t3", t1, new Column[] { c2 });

            // Resulting tables.
            Table t2 = database.getTable("t2");
            Table t3 = database.getTable("t3");
            // View of the original table.
            View v1 = database.getView("t1");

            Row[] rowsOrig = v1.selectAll();
            Assert.assertEquals(1, rowsOrig.length);
            Row[] rowsT2 = t2.selectAll();
            Assert.assertEquals(1, rowsT2.length);
            Row[] rowsT3 = t3.selectAll();
            Assert.assertEquals(1, rowsT3.length);

            Assert.assertEquals(rowsOrig[0].getValue("c1"), rowsT2[0].getValue("c1"));
            Assert.assertEquals(rowsOrig[0].getValue("c2"), rowsT3[0].getValue("c2"));

            Assert.assertEquals(null, rowsT2[0].getValue("c2"));
            Assert.assertEquals(null, rowsT3[0].getValue("c1"));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }
}
