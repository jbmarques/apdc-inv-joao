package apdc.db;

import apdc.db.condition.*;
import apdc.db.exception.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class View {
    private Map<String, ColumnView> columnMap;
    protected String name;
    protected Database database;
    protected Driver driver;

    private static final int INSERT = 0;
    private static final int UPDATE = 1;

    protected View(String name, ColumnView[] columns) {
        this.name = name;
        this.columnMap = new HashMap<>();
        for (ColumnView col : columns) {
            if (col instanceof Column) {
                col = new ColumnView((Column) col);
            }
            columnMap.put(col.getName(), col);
            col.setView(this);
        }
    }

    protected Database getDatabase() {
        return database;
    }

    protected Driver getDriver() {
        return driver;
    }

    protected void setDatabase(Database database) {
        this.database = database;
    }

    protected void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getName() {
        return name;
    }

    public void rename(String name) throws TableNotFoundException, DuplicatedTableException, SQLException {
        if (driver != null) {
            driver.renameTable(this, name);
        }
        if (database != null) {
            database.renameTable(this, name);
        }
        this.name = name;
    }

    public ColumnView getColumn(String colName) {
        return columnMap.get(colName);
    }

    public ColumnView[] getColumns() {
        Collection<ColumnView> cols = columnMap.values();
        return cols.toArray(new ColumnView[cols.size()]);
    }

    public Row[] select(Condition cond) throws SQLException {
        List<Row> rowList = new LinkedList<>();

        if (driver != null) {
            ResultSet result = driver.select(this, cond);
            while (result.next()) {
                Map<String, Object> values = new HashMap<>();
                for (ColumnView column : columnMap.values()) {
                    values.put(column.getName(), DriverPostgreSQL.getObject(result, column));
                }
                rowList.add(new Row(values));
            }
        }
        return rowList.toArray(new Row[rowList.size()]);
    }

    public Row[] selectAll() throws SQLException {
        return select(new Constant<Boolean>(true));
    }

    private void addData(int operation, ColumnView[] columns, Object[] values, Condition cond)
            throws InsufficientColumnsException, ColumnNotFoundException, WrongNumberValuesException,
            InvalidColumnValueException, SQLException {
        if (columns.length == 0)
            throw new InsufficientColumnsException();

        for (ColumnView col : columns) {
            if (!columnMap.containsKey(col.getName()))
                throw new ColumnNotFoundException();
        }
        if (values.length != columns.length)
            throw new WrongNumberValuesException();
        for (int i = 0; i < values.length; i++) {
            if (!Column.validValue(columns[i], values[i]))
                throw new InvalidColumnValueException();
        }

        if (driver != null) {
            if (operation == INSERT)
                driver.insert(this, columns, values);
            else
                driver.update(this, columns, values, cond);
        }
    }

    public void insert(ColumnView[] columns, Object[] values)
            throws InsufficientColumnsException, ColumnNotFoundException, WrongNumberValuesException,
            InvalidColumnValueException, SQLException {
        addData(INSERT, columns, values, null);
    }

    public void update(ColumnView[] columns, Object[] values, Condition cond)
            throws InsufficientColumnsException, ColumnNotFoundException, WrongNumberValuesException,
            InvalidColumnValueException, SQLException {
        addData(UPDATE, columns, values, cond);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof View)) {
            return false;
        }
        View v = (View) o;
        if (database == null || v.database == null || database != v.database) {
            return false;
        }
        return name.equals(v.name);
    }
}
