package apdc.db;

import apdc.db.condition.Condition;
import apdc.db.exception.*;

import java.sql.SQLException;
import java.sql.Types;

public class Column extends ColumnView implements Condition {
    private boolean unique;
    private boolean nullable;
    private boolean primaryKey;
    private Column foreignKeyRef;

    private Table table;
    private Driver driver;

    public Column(String name, int type) throws InvalidTypeException {
        this(name, type, -1);
    }

    public Column(String name, int type, int stringSize) throws InvalidTypeException {
        super(name, type, stringSize);
        this.unique = false;
        this.nullable = true;
        this.primaryKey = false;
        this.foreignKeyRef = null;
        this.table = null;
        this.driver = null;
    }

    public Column(Column col) throws InvalidTypeException, SQLException, ReferentialIntegrityException {
        super(col.name, col.type, col.stringSize);
        this.unique = col.unique;
        this.nullable = col.nullable;
        this.primaryKey = col.primaryKey;
        this.foreignKeyRef = null;
        this.table = null;
        this.driver = null;

        this.setForeignKeyReference(col.foreignKeyRef);
    }

    protected static boolean isCharType(int type) {
        return type == Types.CHAR  || type == Types.VARCHAR;
    }

    protected static boolean validValue(ColumnView col, Object value) {
        if (value == null && (col instanceof Column) && ((Column) col).isNullable())
            return true;

        switch (col.getType()) {
            case Types.BOOLEAN:
                return (value instanceof Boolean);
            case Types.TINYINT:
            case Types.SMALLINT:
            case Types.INTEGER:
                return (value instanceof Integer);
            case Types.BIGINT:
                return (value instanceof Long);
            case Types.FLOAT:
            case Types.DOUBLE:
                return (value instanceof Double);
            case Types.CHAR:
            case Types.VARCHAR:
                return (value instanceof String);
            default:
                return false;
        }
    }

    protected void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    protected Driver getDriver() {
        return driver;
    }

    protected void setDriver(Driver driver) {
        this.driver = driver;
    }

    protected void setTable(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return table;
    }

    public boolean isUnique() {
        return unique;
    }

    public boolean isNullable() {
        return nullable;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public Column getForeignKeyReference() {
        return foreignKeyRef;
    }

    public void rename(String name) throws ColumnNotFoundException, DuplicatedColumnException, SQLException {
        if (driver != null) {
            driver.renameColumn(this, name);
        }
        if (table != null) {
            table.renameColumn(this, name);
        }
        this.name = name;
    }

    public void convert(int type) throws SQLException {
        if (driver != null) {
            driver.convertColumnType(this, type);
        }
        this.type = type;
    }

    public void convert(int type, int stringSize) throws SQLException, InvalidTypeException {
        if (driver != null) {
            driver.convertColumnType(this, type, stringSize);
        }
        if (!isCharType(type) && stringSize != -1) {
            throw new InvalidTypeException("type does not have a string size");
        }
        this.type = type;
        this.stringSize = stringSize;
    }

    public void setUnique(boolean unique) throws SQLException {
        if (unique == this.unique) {
            return;
        }
        if (driver != null) {
            driver.setColumnUniqueConstraint(this, unique);
        }
        this.unique = unique;
    }

    public void setNullable(boolean nullable) throws SQLException {
        if (nullable == this.nullable) {
            return;
        }
        if (driver != null) {
            driver.setColumnNullConstraint(this, nullable);
        }
        this.nullable = nullable;
    }

    public void setForeignKeyReference(Column ref) throws SQLException, ReferentialIntegrityException, InvalidTypeException {
        if (ref != null) {
            if (!ref.isPrimaryKey())
                throw new ReferentialIntegrityException("reference is not primary key");
            if (this.type != ref.getType())
                throw new InvalidTypeException("type differs from reference type");
        }
        if (driver != null)
            driver.setColumnFKConstraint(this, ref);
        if (ref != null)
            ref.table.addReferentialConstraint(ref, this);
        if (foreignKeyRef != null)
            foreignKeyRef.table.dropReferentialConstraint(foreignKeyRef, this);

        foreignKeyRef = ref;
    }

    @Override
    public String toSQL() {
        if (table != null) {
            return table.getName() + "." + name;
        }
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Column)) {
            return false;
        }
        Column c = (Column) o;
        if (table == null || c.table == null || table != c.table) {
            return false;
        }
        return name.equals(c.name);
    }
}
