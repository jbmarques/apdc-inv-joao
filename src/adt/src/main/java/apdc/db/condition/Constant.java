package apdc.db.condition;

public class Constant<T> implements Condition {
    private final T value;

    public Constant(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public String toSQL() {
        if (value instanceof String) {
            return String.format("'%s'", value);
        }
        return value.toString();
    }
}
