package apdc.db.condition;

public interface Condition {
    String toSQL();
}
