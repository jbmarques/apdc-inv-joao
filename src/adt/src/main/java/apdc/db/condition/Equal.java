package apdc.db.condition;

public class Equal implements Condition {
    private final Condition left;
    private final Condition right;

    public Equal(Condition left, Condition right) {
        this.left = left;
        this.right = right;
    }

    public Condition getLeft() {
        return left;
    }

    public Condition getRight() {
        return right;
    }

    public String toSQL() {
        return String.format("(%s = %s)", left.toSQL(), right.toSQL());
    }
}
