package apdc.db.condition;

public class And implements Condition {
    private final Condition left;
    private final Condition right;

    public And(Condition left, Condition right) {
        this.left = left;
        this.right = right;
    }

    public Condition getLeft() {
        return left;
    }

    public Condition getRight() {
        return right;
    }

    public String toSQL() {
        return String.format("(%s AND %s)", left.toSQL(), right.toSQL());
    }
}
