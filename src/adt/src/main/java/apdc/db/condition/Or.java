package apdc.db.condition;

public class Or implements Condition {
    private final Condition left;
    private final Condition right;

    public Or(Condition left, Condition right) {
        this.left = left;
        this.right = right;
    }

    public Condition getLeft() {
        return left;
    }

    public Condition getRight() {
        return right;
    }

    public String toSQL() {
        return String.format("(%s OR %s)", left.toSQL(), right.toSQL());
    }
}
