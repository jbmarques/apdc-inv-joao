package apdc.db;

import apdc.db.condition.Condition;
import java.sql.SQLException;
import java.sql.ResultSet;

public interface Driver {
    Table[] getTables() throws SQLException;

    View[] getViews() throws SQLException;

    void createTable(Table table) throws SQLException;

    void dropTable(Table table) throws SQLException;

    void dropView(View view) throws SQLException;

    <T extends View>void renameTable(T table, String newName) throws SQLException;

    void addColumn(Table table, Column column) throws SQLException;

    void dropColumn(Column column) throws SQLException;

    void setTablePrimaryKey(Table table, Column[] columns) throws SQLException;

    void convertColumnType(Column column, int type) throws SQLException;

    void convertColumnType(Column column, int type, int stringSize) throws SQLException;

    void setColumnFKConstraint(Column column, Column fk) throws SQLException;

    void setColumnUniqueConstraint(Column column, boolean unique) throws SQLException;

    void setColumnNullConstraint(Column column, boolean nullable) throws SQLException;

    <T extends View>ResultSet select(T table, Condition cond) throws SQLException;

    <T extends View>ResultSet selectAll(T table) throws SQLException;

    void insert(View table, ColumnView[] columns, Object[] values) throws SQLException;

    void update(View table, ColumnView[] columns, Object[] values, Condition cond) throws SQLException;

    void innerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException;

    void outerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException;

    void innerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException;

    void outerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException;

    void splitTable(String t1, String t2, Table table, Column[] t1Columns, Column[] t2Columns) throws SQLException;

    void splitTableIntoTables(String t1, String t2, Table table, Column[] t1Columns, Column[] t2Columns) throws SQLException;

    void renameColumn(Column column, String newName) throws SQLException;

    void close() throws SQLException;
}
