package apdc.db;

import apdc.db.exception.InvalidTypeException;

public class ColumnView {
    protected String name;
    protected int type;
    protected int stringSize;
    private View view;

    protected ColumnView(Column column) {
        this.name = column.getName();
        this.type = column.getType();
        this.stringSize = column.getStringSize();
        this.view = null;
    }

    protected ColumnView(String name, int type) throws InvalidTypeException {
        this(name, type, -1);
    }

    protected ColumnView(String name, int type, int stringSize) throws InvalidTypeException {
        if (!Column.isCharType(type) && stringSize != -1) {
            throw new InvalidTypeException("type does not have a string size");
        }
        this.name = name;
        this.type = type;
        this.stringSize = stringSize;
        this.view = null;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public int getStringSize() {
        return stringSize;
    }

    public View getView() {
        return view;
    }

    protected void setView(View view) {
        this.view = view;
    }
}
