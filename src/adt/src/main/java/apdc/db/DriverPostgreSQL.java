package apdc.db;

import apdc.db.condition.Condition;
import apdc.db.exception.*;

import java.io.PrintStream;
import java.sql.*;
import java.util.*;

public class DriverPostgreSQL implements Driver {
    private Connection conn;
    private boolean dryRun;
    private PrintStream out;
    private long indexCounter;

    protected DriverPostgreSQL(String url, Properties props, boolean dryRun, PrintStream out) throws SQLException {
        this.conn = DriverManager.getConnection(url, props);
        this.dryRun = dryRun;
        this.out = out;
        this.indexCounter = 1;
    }

    private void dispatchUpdate(String update) throws SQLException {
        if (!dryRun) {
            Statement st = conn.createStatement();
            st.executeUpdate(update);
            st.close();
        }
        if (out != null)
            out.println(update);
    }

    private ResultSet dispatchQuery(String query) throws SQLException {
        ResultSet rs = null;

        if (!dryRun) {
            Statement st = conn.createStatement();
            rs = st.executeQuery(query);
        }
        return rs;
    }

    public Table[] getTables() throws SQLException {
        Map<String, Table> tables = new HashMap<>();
        DatabaseMetaData md = conn.getMetaData();

        try {
            ResultSet rs = md.getTables(null, null, "%", new String[]{"TABLE"});
            while (rs.next()) {
                String tableName = rs.getString(3);
                Column[] columns = getTableColumns(md, tableName);
                tables.put(tableName, new Table(tableName, columns));
            }
            rs.close();
            for (Table table : tables.values()) {
                rs = md.getImportedKeys(null, null, table.getName());
                while (rs.next()) {
                    Table refT = tables.get(rs.getString(3));
                    Column ref = refT.getColumn(rs.getString(4));
                    Column col = table.getColumn(rs.getString(8));
                    col.setForeignKeyReference(ref);
                }
                rs.close();
            }
        } catch (InvalidTypeException         |
                ReferentialIntegrityException |
                DuplicatedColumnException     |
                OperationNotAllowedException e) {
            throw new SQLException(e.getMessage());
        }

        Collection<Table> ts = tables.values();
        return ts.toArray(new Table[ts.size()]);
    }

    @Override
    public View[] getViews() throws SQLException {
        List<View> views = new LinkedList<>();
        DatabaseMetaData md = conn.getMetaData();

        try {
            ResultSet viewsResult = md.getTables(null, null, "%", new String[] { "VIEW" });
            while (viewsResult.next()) {
                String viewName = viewsResult.getString(3);
                List<ColumnView> columns = new LinkedList<>();

                ResultSet colsResult = md.getColumns(null, null, viewName, "%");
                while (colsResult.next()) {
                    ColumnView col = null;
                    int type = colsResult.getInt(5);
                    if (Column.isCharType(5)) {
                        col = new ColumnView(colsResult.getString(4), type, colsResult.getInt(16));
                    } else {
                        col = new ColumnView(colsResult.getString(4), type);
                    }
                    columns.add(col);
                }
                colsResult.close();
                views.add(new View(viewName, columns.toArray(new ColumnView[columns.size()])));
            }
        } catch (InvalidTypeException e) {
            throw new SQLException(e.getMessage());
        }
        return views.toArray(new View[views.size()]);
    }

    private Column[] getTableColumns(DatabaseMetaData md, String tableName)
            throws SQLException, InvalidTypeException, OperationNotAllowedException {
        Map<String, Column> columns = new HashMap<>();

        ResultSet rs = md.getColumns(null, null, tableName, "%");
        while (rs.next()) {
            Column col = null;
            int type = rs.getInt(5);
            if (Column.isCharType(type)) {
                col = new Column(rs.getString(4), type, rs.getInt(16));
            } else {
                col = new Column(rs.getString(4), type);
            }
            columns.put(col.getName(), col);
        }
        rs.close();
        rs = md.getPrimaryKeys(null, null, tableName);
        while (rs.next()) {
            columns.get(rs.getString(4)).setPrimaryKey(true);
        }
        rs.close();

        Collection<Column> cs = columns.values();
        return cs.toArray(new Column[cs.size()]);
    }

    @Override
    public void createTable(Table table) throws SQLException {
        StringBuilder update = new StringBuilder();
        List<String> primaryKeys = new LinkedList<>();

        update.append(String.format("CREATE TABLE %s (", table.getName()));
        for (Column column : table.getColumns()) {
            update.append(ColumnToSQL(column, false) + ",");

            if (column.isPrimaryKey())
                primaryKeys.add(column.getName());
        }
        update.append(String.format("PRIMARY KEY (%s));", String.join(",", primaryKeys)));
        dispatchUpdate(update.toString());
    }

    @Override
    public void dropTable(Table table) throws SQLException {
        String update = String.format("DROP TABLE %s;", table.getName());
        dispatchUpdate(update);
    }

    @Override
    public void dropView(View view) throws SQLException {
        String update = String.format("DROP VIEW %s;", view.getName());
        dispatchUpdate(update);
    }

    @Override
    public <T extends View> void renameTable(T table, String newName) throws SQLException {
        String update = String.format("ALTER TABLE %s RENAME TO %s;", table.getName(), newName);
        dispatchUpdate(update);
    }

    @Override
    public void addColumn(Table table, Column column) throws SQLException {
        String update = String.format("ALTER TABLE %s ADD COLUMN %s;",
                table.getName(), ColumnToSQL(column, true));

        dispatchUpdate(update);
    }

    @Override
    public void dropColumn(Column column) throws SQLException {
        String update = String.format("ALTER TABLE %s DROP COLUMN %s;",
                column.getTable().getName(), column.getName());

        dispatchUpdate(update);
    }

    @Override
    public void setTablePrimaryKey(Table table, Column[] columns) throws SQLException {
        String constraintName = table.getName() + "_pkey";
        String indexName = String.format("%s_pkey_idx_%d", table.getName(), indexCounter++);
        String[] columnNames = new String[columns.length];

        for (int i = 0; i < columns.length; i++)
            columnNames[i] = columns[i].getName();

        String indexUpdate = String.format("CREATE UNIQUE INDEX CONCURRENTLY %s ON %s (%s);",
                indexName, table.getName(), String.join(",", columnNames));
        dispatchUpdate(indexUpdate);

        String update = String.format("ALTER TABLE %s DROP CONSTRAINT %s, ADD CONSTRAINT %s PRIMARY KEY USING INDEX %s;",
                table.getName(), constraintName, constraintName, indexName);
        dispatchUpdate(update);
    }

    @Override
    public void convertColumnType(Column column, int type) throws SQLException {
        String update = String.format("ALTER TABLE %s ALTER COLUMN %s TYPE %s;",
                column.getTable().getName(), column.getName(), SQLTypeToString(type));

        dispatchUpdate(update);
    }

    @Override
    public void convertColumnType(Column column, int type, int stringSize) throws SQLException {
        String update = String.format("ALTER TABLE %s ALTER COLUMN %s TYPE %s(%d);",
                column.getTable().getName(), column.getName(), SQLTypeToString(type), stringSize);

        dispatchUpdate(update);
    }

    @Override
    public void setColumnFKConstraint(Column column, Column fk) throws SQLException {
        String tableName = column.getTable().getName();
        String constraintName = String.format("%s_%s_fkey", tableName, column.getName());

        String dropUpdate = String.format("ALTER TABLE %s DROP CONSTRAINT IF EXISTS %s;", tableName, constraintName);
        dispatchUpdate(dropUpdate);

        if (fk != null) {
            String addUpdate = String.format("ALTER TABLE %s ADD CONSTRAINT %s FOREIGN KEY (%s) REFERENCES %s (%s);",
                    tableName, constraintName, column.getName(), fk.getTable().getName(), fk.getName());
            dispatchUpdate(addUpdate);
        }
    }

    @Override
    public void setColumnUniqueConstraint(Column column, boolean unique) throws SQLException {
        String tableName = column.getTable().getName();
        String constraintName = String.format("%s_%s_key", tableName, column.getName());

        String dropUpdate = String.format("ALTER TABLE %s DROP CONSTRAINT IF EXISTS %s;", tableName, constraintName);
        dispatchUpdate(dropUpdate);

        String addUpdate = String.format("ALTER TABLE %s ADD CONSTRAINT %s UNIQUE (%s);",
                tableName, constraintName, column.getName());
        dispatchUpdate(addUpdate);
    }

    @Override
    public void setColumnNullConstraint(Column column, boolean nullable) throws SQLException {
        String update = String.format("ALTER TABLE %s ALTER COLUMN %s %s NOT NULL;",
                column.getTable().getName(), column.getName(), (nullable) ? "DROP" : "SET");

        dispatchUpdate(update);
    }

    @Override
    public <T extends View> ResultSet select(T table, Condition cond) throws SQLException {
        String query = String.format("SELECT * FROM %s WHERE %s;", table.getName(), cond.toSQL());
        return dispatchQuery(query);
    }

    @Override
    public <T extends View> ResultSet selectAll(T table) throws SQLException {
        String query = String.format("SELECT * FROM %s;", table.getName());
        return dispatchQuery(query);
    }

    @Override
    public void insert(View table, ColumnView[] columns, Object[] values) throws SQLException {
        String[] columnNames = new String[columns.length];
        String[] vals = new String[values.length];

        for (int i = 0; i < columns.length; i++) {
            columnNames[i] = columns[i].getName();
            vals[i] = columnValueToString(values[i]);
        }
        String update = String.format("INSERT INTO %s (%s) VALUES (%s);",
                table.getName(), String.join(",", columnNames), String.join(",", vals));

        dispatchUpdate(update);
    }

    @Override
    public void update(View table, ColumnView[] columns, Object[] values, Condition cond) throws SQLException {
        String[] columnValues = new String[columns.length];

        for (int i = 0; i < columns.length; i++)
            columnValues[i] = String.format("%s=%s", columns[i].getName(), columnValueToString(values[i]));

        String update = String.format("UPDATE %s SET %s WHERE %s;",
                table.getName(), String.join(",", columnValues), cond.toSQL());

        dispatchUpdate(update);
    }

    @Override
    public void innerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException {
        String[] columnNames = new String[cols.length];

        for (int i = 0; i < columnNames.length; i++)
            columnNames[i] = cols[i].getTable().getName() + "." + cols[i].getName();

        String update = String.format("CREATE VIEW %s AS SELECT %s FROM %s INNER JOIN %s ON %s;",
                name, String.join(",", columnNames), t1.getName(), t2.getName(), cond.toSQL());

        dispatchUpdate(update);
    }

    @Override
    public void outerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException {
        String[] columnNames = new String[cols.length];

        for (int i = 0; i < columnNames.length; i++)
            columnNames[i] = cols[i].getTable().getName() + "." + cols[i].getName();

        String update = String.format("CREATE VIEW %s AS SELECT %s FROM %s OUTER JOIN %s ON %s;",
                name, String.join(",", columnNames), t1.getName(), t2.getName(), cond.toSQL());

        dispatchUpdate(update);
    }

    @Override
    public void innerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException {
        String[] columnNames = new String[cols.length];

        for (int i = 0; i < columnNames.length; i++)
            columnNames[i] = cols[i].getTable().getName() + "." + cols[i].getName();

        String update = String.format("CREATE TABLE %s AS SELECT %s FROM %s INNER JOIN %s ON %s;",
                name, String.join(",", columnNames), t1.getName(), t2.getName(), cond.toSQL());

        dispatchUpdate(update);
    }

    @Override
    public void outerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond) throws SQLException {
        String[] columnNames = new String[cols.length];

        for (int i = 0; i < columnNames.length; i++)
            columnNames[i] = cols[i].getTable().getName() + "." + cols[i].getName();

        String update = String.format("CREATE TABLE %s AS SELECT %s FROM %s OUTER JOIN %s ON %s;",
                name, String.join(",", columnNames), t1.getName(), t2.getName(), cond.toSQL());

        dispatchUpdate(update);
    }

    @Override
    public void splitTable(String t1, String t2, Table table, Column[] t1Columns, Column[] t2Columns) throws SQLException {
        String[] t1Cols = new String[t1Columns.length];
        String[] t2Cols = new String[t2Columns.length];

        for (int i = 0; i < t1Columns.length; i++)
            t1Cols[i] = t1Columns[i].getName();
        for (int i = 0; i < t2Columns.length; i++)
            t2Cols[i] = t2Columns[i].getName();

        String view1 = String.format("CREATE VIEW %s AS SELECT %s FROM %s;",
                t1, String.join(",", t1Cols), table.getName());
        String view2 = String.format("CREATE VIEW %s AS SELECT %s FROM %s;",
                t2, String.join(",", t2Cols), table.getName());

        dispatchUpdate(view1);
        dispatchUpdate(view2);
    }

    @Override
    public void splitTableIntoTables(String t1, String t2, Table table, Column[] t1Columns, Column[] t2Columns) throws SQLException {
        String[] t1Cols = new String[t1Columns.length];
        String[] t2Cols = new String[t2Columns.length];

        for (int i = 0; i < t1Columns.length; i++)
            t1Cols[i] = t1Columns[i].getName();
        for (int i = 0; i < t2Columns.length; i++)
            t2Cols[i] = t2Columns[i].getName();

        String table1 = String.format("CREATE TABLE %s AS SELECT %s FROM %s;",
                t1, String.join(",", t1Cols), table.getName());
        String table2 = String.format("CREATE TABLE %s AS SELECT %s FROM %s;",
                t2, String.join(",", t2Cols), table.getName());

        dispatchUpdate(table1);
        dispatchUpdate(table2);
    }

    @Override
    public void renameColumn(Column column, String newName) throws SQLException {
        String tableName = column.getTable().getName();
        String oldName = column.getName();

        String update = String.format("ALTER TABLE %s RENAME COLUMN %s TO %s;", tableName, oldName, newName);
        dispatchUpdate(update);
    }

    @Override
    public void close() throws SQLException {
        conn.close();
    }

    private static String ColumnToSQL(Column column, boolean withPK) {
        StringBuilder sb = new StringBuilder();

        String name = column.getName();
        String type = SQLTypeToString(column.getType());
        Column fk = column.getForeignKeyReference();

        sb.append(String.format("%s %s", name, type));
        if (Column.isCharType(column.getType()))
            sb.append(String.format("(%d)", column.getStringSize()));
        if (withPK && column.isPrimaryKey())
            sb.append(" PRIMARY KEY");
        else if (fk != null)
            sb.append(String.format(" REFERENCES %s (%s)", fk.getTable().getName(), fk.getName()));

        return sb.toString();
    }

    protected static String SQLTypeToString(int type) {
        switch (type) {
            case Types.BOOLEAN  : return "BOOLEAN";
            case Types.DOUBLE   : return "DOUBLE";
            case Types.FLOAT    : return "FLOAT";
            case Types.TINYINT  : return "TINYINT";
            case Types.SMALLINT : return "SMALLINT";
            case Types.INTEGER  : return "INT";
            case Types.BIGINT   : return "BIGINT";
            case Types.CHAR     : return "CHAR";
            case Types.VARCHAR  : return "VARCHAR";
            default:
                throw new RuntimeException(String.format("Unsupported type '%d'", type));
        }
    }

    protected static Object getObject(ResultSet rs, ColumnView column) throws SQLException {
        switch (column.getType()) {
            case Types.BOOLEAN:
                return rs.getBoolean(column.getName());
            case Types.TINYINT:
            case Types.SMALLINT:
            case Types.INTEGER:
                return rs.getInt(column.getName());
            case Types.BIGINT:
                return rs.getLong(column.getName());
            case Types.FLOAT:
            case Types.DOUBLE:
                return rs.getDouble(column.getName());
            case Types.CHAR:
            case Types.VARCHAR:
                return rs.getString(column.getName());
            default:
                throw new RuntimeException(String.format("Unsupported type '%d'", column.getType()));
        }
    }

    protected static String columnValueToString(Object value) {
        if (value == null)
            return "NULL";
        if (value instanceof Boolean)
            return ((Boolean) value) ? "TRUE" : "FALSE";
        if (value instanceof String)
            return "'" + value.toString() + "'";
        return value.toString();
    }
}
