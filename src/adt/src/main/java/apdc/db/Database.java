package apdc.db;

import apdc.db.condition.*;
import apdc.db.exception.*;

import java.io.PrintStream;
import java.sql.SQLException;
import java.util.*;

public class Database {
    private Driver driver;
    private Map<String, View> tableMap;
    private Map<String, Set<View>> viewsReferentialIntegrity;

    private static final int INNER_JOIN = 0;
    private static final int OUTER_JOIN = 1;

    public Database(String url, boolean dryRun) throws SQLException {
        this(url, null, null, dryRun, null);
    }

    public Database(String url, boolean dryRun, PrintStream out) throws SQLException {
        this(url, null, null, dryRun, out);
    }

    public Database(String url, String user, String password, boolean dryRun) throws SQLException {
        this(url, user, password, dryRun, null);
    }

    public Database(String url, String user, String password, boolean dryRun, PrintStream out) throws SQLException {
        Properties props = new Properties();
        if (user != null) {
            props.setProperty("user", user);
        }
        if (password != null) {
            props.setProperty("password", password);
        }
        this.tableMap = new HashMap<>();
        this.viewsReferentialIntegrity = new HashMap<>();

        Driver driver = new DriverPostgreSQL(url, props, dryRun, out);
        try {
            for (Table table : driver.getTables())
                createTable(table);
            for (View view : driver.getViews()) {
                tableMap.put(view.getName(), view);
                view.setDatabase(this);
            }
        } catch (TableAlreadyBoundException | DuplicatedTableException e) {
            throw new SQLException(e.getMessage());
        }
        this.driver = driver;
        for (View t : tableMap.values())
            t.setDriver(driver);
    }

    private <T extends View> T addTable(T table) {
        T result = (T)(tableMap.put(table.getName(), table));
        table.setDatabase(this);
        table.setDriver(driver);
        return result;
    }

    private View removeView(View view) {
        View result = (View)(tableMap.remove(view.getName()));
        view.setDatabase(null);
        view.setDriver(null);
        return result;
    }

    private Table removeTable(Table table) {
        // Remove references to foreign key constraints.
        for (Column c : table.getColumns()) {
            Column ref = c.getForeignKeyReference();
            if (ref != null)
                ref.getTable().dropReferentialConstraint(ref, c);
        }

        Table result = (Table)(tableMap.remove(table.getName()));
        table.setDatabase(null);
        table.setDriver(null);
        return result;
    }

    private boolean tableExists(String tableName) {
        return tableMap.containsKey(tableName);
    }

    private <T extends View> boolean tableExists(T table) {
        return (table.getDatabase() == this) && (tableMap.containsKey(table.getName()));
    }

    protected boolean isReferenced(Table table) {
        Set<View> refs = viewsReferentialIntegrity.get(table.getName());
        return refs != null && !refs.isEmpty();
    }

    private void addReferentialConstraint(Table table, View view) {
        Set<View> refs = viewsReferentialIntegrity.get(table.getName());
        if (refs == null) {
            refs = new HashSet<>();
            viewsReferentialIntegrity.put(table.getName(), refs);
        }
        refs.add(view);
    }

    protected void dropReferentialConstraint(Table table, View view) {
        Set<View> refs = viewsReferentialIntegrity.get(table.getName());
        if (refs != null)
            refs.remove(view);
    }

    private Column[] getJoinColumns(Table t1, Table t2) {
        List<Column> cols = new LinkedList<>();
        Set<String> names = new HashSet<>();

        for (Column c : t1.getColumns()) {
            Column fk = c.getForeignKeyReference();
            if (fk == null || fk.getTable() != t2) {
                cols.add(c);
                names.add(c.getName());
            }
        }
        for (Column c : t2.getColumns()) {
            Column fk = c.getForeignKeyReference();
            if (!names.contains(c.getName()) && (fk == null || fk.getTable() != t1))
                cols.add(c);
        }
        return cols.toArray(new Column[cols.size()]);
    }

    protected void renameTable(View table, String newName) throws DuplicatedTableException, TableNotFoundException {
        if (tableExists(newName)) {
            throw new DuplicatedTableException(newName);
        }
        if (tableMap.remove(table.getName()) == null) {
            throw new TableNotFoundException(table.getName());
        }
        tableMap.put(newName, table);
    }

    public void close() throws SQLException {
        for (View t : tableMap.values()) {
            t.setDatabase(null);
            t.setDriver(null);
        }
        tableMap = null;
        driver.close();
        driver = null;
    }

    public View getView(String viewName) {
        View v = tableMap.get(viewName);

        if (!(v instanceof Table)) {
            return v;
        }
        return null;
    }

    public View[] getViews() {
        List<View> views = new LinkedList<>();

        for (View v : tableMap.values()) {
            if (!(v instanceof Table))
                views.add(v);
        }
        return views.toArray(new View[views.size()]);
    }

    public Table getTable(String tableName) {
        View t = tableMap.get(tableName);

        if (t instanceof Table) {
            return ((Table) t);
        }
        return null;
    }

    public Table[] getTables() {
        List<Table> tables = new LinkedList<>();

        for (View t : tableMap.values()) {
            if (t instanceof Table)
                tables.add((Table) t);
        }
        return tables.toArray(new Table[tables.size()]);
    }

    public void createTable(Table table) throws DuplicatedTableException, TableAlreadyBoundException, SQLException {
        if (tableExists(table)) {
            throw new DuplicatedTableException(table.getName());
        }
        if (table.getDatabase() != null) {
            throw new TableAlreadyBoundException(table.getName());
        }
        if (driver != null) {
            driver.createTable(table);
        }
        addTable(table);
    }

    public <T extends View> void dropTable(T table)
            throws TableNotFoundException, ReferentialIntegrityException, SQLException {
        if (!tableExists(table)) {
            throw new TableNotFoundException(table.getName());
        }
        // Check referential integrity if we are dropping a table and not a view.
        if (table instanceof Table) {
            Table t = ((Table) table);
            // Check referential integrity on foreign keys.
            for (Column c : t.getColumns()) {
                if (t.isReferenced(c))
                    throw new ReferentialIntegrityException(c.getName());
            }
            // Check referential integrity on view.
            if (isReferenced(t))
                throw new ReferentialIntegrityException();
        }

        if (driver != null) {
            if (table instanceof Table) {
                driver.dropTable((Table) table);
            } else {
                driver.dropView(table);
                // Drop the referential constraint if the view references a table.
                for (View t : tableMap.values()) {
                    if (t instanceof Table)
                        dropReferentialConstraint((Table) t, table);
                }
            }
        }

        if (table instanceof Table)
            removeTable((Table) table);
        else
            removeView(table);
    }

    protected void joinTables(int join, String name, Table t1, Table t2, Condition cond)
            throws SQLException, TableNotFoundException, DuplicatedTableException, NoJoinColumnsFoundException {
        if (!tableExists(t1) || !tableExists(t2))
            throw new TableNotFoundException();
        if (tableExists(name))
            throw new DuplicatedTableException(name);

        Column[] cols = getJoinColumns(t1, t2);
        if (cols.length == 0)
            throw new NoJoinColumnsFoundException();

        joinTables(join, name, cols, t1, t2, cond);
    }

    protected void joinTables(int join, String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, TableNotFoundException, DuplicatedTableException {
        if (!tableExists(t1) || !tableExists(t2))
            throw new TableNotFoundException();
        if (tableExists(name))
            throw new DuplicatedTableException(name);

        if (driver != null) {
            if (join == INNER_JOIN)
                driver.innerJoinTables(name, cols, t1, t2, cond);
            else
                driver.outerJoinTables(name, cols, t1, t1, cond);
        }
        View view = new View(name, cols);
        addTable(view);
        addReferentialConstraint(t1, view);
        addReferentialConstraint(t2, view);
    }

    protected void joinTablesIntoTable(int join, String name, Table t1, Table t2, Condition cond)
            throws SQLException, TableNotFoundException, DuplicatedTableException, NoJoinColumnsFoundException, ReferentialIntegrityException, DuplicatedColumnException, ColumnNotFoundException {
        if (!tableExists(t1) || !tableExists(t2))
            throw new TableNotFoundException();
        if (tableExists(name))
            throw new DuplicatedTableException(name);

        Column[] cols = getJoinColumns(t1, t2);
        if (cols.length == 0)
            throw new NoJoinColumnsFoundException();

        joinTablesIntoTable(join, name, cols, t1, t2, cond);
    }

    protected void joinTablesIntoTable(int join, String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, TableNotFoundException, DuplicatedTableException, DuplicatedColumnException, ColumnNotFoundException, ReferentialIntegrityException {
        if (!tableExists(t1) || !tableExists(t2))
            throw new TableNotFoundException();
        if (tableExists(name))
            throw new DuplicatedTableException(name);

        if (driver != null) {
            if (join == INNER_JOIN)
                driver.innerJoinTablesIntoTable(name, cols, t1, t2, cond);
            else
                driver.outerJoinTablesIntoTable(name, cols, t1, t1, cond);
        }

        Table table = new Table(name, cols);
        addTable(table);

        try {
            dropTable(t1);
            dropTable(t2);
        } catch (ReferentialIntegrityException e) {
            dropTable(t2);
            dropTable(t1);
        }

        List<Column> t2Columns = new LinkedList<>();
        for (Column c : t2.getColumns()) {
            Column col = table.getColumn(c.getName());
            if (col != null) {
                t2Columns.add(col);
            }
        }

        // Foreign key columns will get the name of the column they reference, this could be improved.
        splitTable(t1.getName(), t2.getName(), table, t2Columns.toArray(new Column[t2Columns.size()]));
    }

    public void innerJoinTables(String name, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, NoJoinColumnsFoundException {
        joinTables(INNER_JOIN, name, t1, t2, cond);
    }

    public void innerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException {
        joinTables(INNER_JOIN, name, cols, t1, t2, cond);
    }

    public void outerJoinTables(String name, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, NoJoinColumnsFoundException {
        joinTables(OUTER_JOIN, name, t1, t2, cond);
    }

    public void outerJoinTables(String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException {
        joinTables(OUTER_JOIN, name, cols, t1, t2, cond);
    }

    public void innerJoinTablesIntoTable(String name, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, NoJoinColumnsFoundException, ReferentialIntegrityException, DuplicatedColumnException, ColumnNotFoundException {
        joinTablesIntoTable(INNER_JOIN, name, t1, t2, cond);
    }

    public void innerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, ReferentialIntegrityException, DuplicatedColumnException, ColumnNotFoundException {
        joinTablesIntoTable(INNER_JOIN, name, cols, t1, t2, cond);
    }

    public void outerJoinTablesIntoTable(String name, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, NoJoinColumnsFoundException, ReferentialIntegrityException, DuplicatedColumnException, ColumnNotFoundException {
        joinTablesIntoTable(OUTER_JOIN, name, t1, t2, cond);
    }

    public void outerJoinTablesIntoTable(String name, Column[] cols, Table t1, Table t2, Condition cond)
            throws SQLException, DuplicatedTableException, TableNotFoundException, ReferentialIntegrityException, DuplicatedColumnException, ColumnNotFoundException {
        joinTablesIntoTable(OUTER_JOIN, name, cols, t1, t2, cond);
    }

    public void splitTable(String name1, String name2, Table table, Column[] columns)
            throws SQLException, TableNotFoundException, DuplicatedTableException, ColumnNotFoundException {
        if (!tableExists(table))
            throw new TableNotFoundException(table.getName());
        if (tableExists(name1) || tableExists(name2))
            throw new DuplicatedTableException();

        List<Column> columnsList = Arrays.asList(columns);

        for (Column col : columnsList) {
            if (col.getTable() != table)
                throw new ColumnNotFoundException(col.getName());
        }

        Set<Column> v1Columns = new HashSet<>();
        Set<Column> v2Columns = new HashSet<>();
        for (Column col : table.getColumns()) {
            if (col.isPrimaryKey()) {
                v1Columns.add(col);
                v2Columns.add(col);
            } else if (columnsList.contains(col)) {
                v2Columns.add(col);
            } else {
                v1Columns.add(col);
            }
        }

        Column[] v1ColsArray = v1Columns.toArray(new Column[v1Columns.size()]);
        Column[] v2ColsArray = v2Columns.toArray(new Column[v2Columns.size()]);
        if (driver != null) {
            driver.splitTable(name1, name2, table, v1ColsArray, v2ColsArray);
        }
        View v1 = new View(name1, v1ColsArray);
        View v2 = new View(name2, v2ColsArray);
        addTable(v1);
        addTable(v2);
        addReferentialConstraint(table, v1);
        addReferentialConstraint(table, v2);
    }

    public void splitTableIntoTables(String name1, String name2, Table table, Column[] columns)
            throws SQLException, TableNotFoundException, DuplicatedTableException, ColumnNotFoundException, DuplicatedColumnException, ColumnAlreadyBoundException, ReferentialIntegrityException, NoJoinColumnsFoundException {
        if (!tableExists(table))
            throw new TableNotFoundException(table.getName());
        if (tableExists(name1) || tableExists(name2))
            throw new DuplicatedTableException();

        List<Column> columnsList = Arrays.asList(columns);

        for (Column col : columnsList) {
            if (col.getTable() != table)
                throw new ColumnNotFoundException(col.getName());
        }

        Set<Column> v1Columns = new HashSet<>();
        Set<Column> v2Columns = new HashSet<>();
        Set<Column> pkColumns = new HashSet<>();
        for (Column col : table.getColumns()) {
            if (col.isPrimaryKey()) {
                v1Columns.add(col);
                v2Columns.add(col);
                pkColumns.add(col);
            } else if (columnsList.contains(col)) {
                v2Columns.add(col);
            } else {
                v1Columns.add(col);
            }
        }

        Column[] t1ColsArray = v1Columns.toArray(new Column[v1Columns.size()]);
        Column[] t2ColsArray = v2Columns.toArray(new Column[v2Columns.size()]);
        if (driver != null) {
            driver.splitTableIntoTables(name1, name2, table, t1ColsArray, t2ColsArray);
        }

        // Resulting tables from the split operation.
        Table t1 = new Table(name1, t1ColsArray);
        Table t2 = new Table(name2, t2ColsArray);

        addTable(t1);
        addTable(t2);

        // Create predicate that joins the resulting tables into a view of the original.
        Queue<Condition> equalList = new LinkedList<>();
        for (Column pk : pkColumns) {
            String pkName = pk.getName();
            equalList.add(new Equal(t1.getColumn(pkName), t2.getColumn(pkName)));
        }
        if (equalList.isEmpty()) {
            throw new RuntimeException("Cannot join resulting tables");
        }
        Condition cond = equalList.remove();
        while (!equalList.isEmpty()) {
            cond = new And(cond, equalList.remove());
        }

        dropTable(table);

        innerJoinTables(table.getName(), t1, t2, cond);
    }
}
