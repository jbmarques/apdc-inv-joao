package apdc.db.exception;

public class ReferentialIntegrityException extends Exception {
    public ReferentialIntegrityException() {
        super();
    }

    public ReferentialIntegrityException(String message) {
        super(message);
    }

    public ReferentialIntegrityException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReferentialIntegrityException(Throwable cause) {
        super(cause);
    }
}
