package apdc.db.exception;

public class TableAlreadyBoundException extends Exception {
    public TableAlreadyBoundException() {
        super();
    }

    public TableAlreadyBoundException(String message) {
        super(message);
    }

    public TableAlreadyBoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TableAlreadyBoundException(Throwable cause) {
        super(cause);
    }
}
