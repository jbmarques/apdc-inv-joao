package apdc.db.exception;

public class DuplicatedTableException extends Exception {
    public DuplicatedTableException() {
        super();
    }

    public DuplicatedTableException(String message) {
        super(message);
    }

    public DuplicatedTableException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatedTableException(Throwable cause) {
        super(cause);
    }
}
