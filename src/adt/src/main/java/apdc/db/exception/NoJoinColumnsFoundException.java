package apdc.db.exception;

public class NoJoinColumnsFoundException extends Exception {
    public NoJoinColumnsFoundException() {
        super();
    }

    public NoJoinColumnsFoundException(String message) {
        super(message);
    }

    public NoJoinColumnsFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoJoinColumnsFoundException(Throwable cause) {
        super(cause);
    }
}
