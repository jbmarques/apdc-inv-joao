package apdc.db.exception;

public class DuplicatedColumnException extends Exception {
    public DuplicatedColumnException() {
        super();
    }

    public DuplicatedColumnException(String message) {
        super(message);
    }

    public DuplicatedColumnException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatedColumnException(Throwable cause) {
        super(cause);
    }
}
