package apdc.db.exception;

public class InsufficientColumnsException extends Exception {
    public InsufficientColumnsException() {
        super();
    }

    public InsufficientColumnsException(String message) {
        super(message);
    }

    public InsufficientColumnsException(String message, Throwable cause) {
        super(message, cause);
    }

    public InsufficientColumnsException(Throwable cause) {
        super(cause);
    }
}