package apdc.db.exception;

public class TableNotFoundException extends Exception {
    public TableNotFoundException() {
        super();
    }

    public TableNotFoundException(String message) {
        super(message);
    }

    public TableNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TableNotFoundException(Throwable cause) {
        super(cause);
    }
}
