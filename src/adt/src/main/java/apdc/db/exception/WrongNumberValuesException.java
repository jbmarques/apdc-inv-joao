package apdc.db.exception;

public class WrongNumberValuesException extends Exception {
    public WrongNumberValuesException() {
        super();
    }

    public WrongNumberValuesException(String message) {
        super(message);
    }

    public WrongNumberValuesException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongNumberValuesException(Throwable cause) {
        super(cause);
    }
}
