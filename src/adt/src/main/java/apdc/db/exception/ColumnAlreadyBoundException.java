package apdc.db.exception;

public class ColumnAlreadyBoundException extends Exception {
    public ColumnAlreadyBoundException() {
        super();
    }

    public ColumnAlreadyBoundException(String message) {
        super(message);
    }

    public ColumnAlreadyBoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ColumnAlreadyBoundException(Throwable cause) {
        super(cause);
    }
}
