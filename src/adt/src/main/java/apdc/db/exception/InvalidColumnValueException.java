package apdc.db.exception;

public class InvalidColumnValueException extends Exception {
    public InvalidColumnValueException() {
        super();
    }

    public InvalidColumnValueException(String message) {
        super(message);
    }

    public InvalidColumnValueException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidColumnValueException(Throwable cause) {
        super(cause);
    }
}