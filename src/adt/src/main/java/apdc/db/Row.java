package apdc.db;

import java.util.HashMap;
import java.util.Map;

public class Row {
    private Map<String, Object> values;

    protected Row(Map<String, Object> values) {
        this.values = new HashMap<>();
        for (Map.Entry<String, Object> e : values.entrySet())
            this.values.put(e.getKey(), e.getValue());
    }

    public Object getValue(String columnName) {
        return values.get(columnName);
    }
}
