package apdc.db;

import apdc.db.exception.*;

import java.sql.SQLException;
import java.util.*;

public class Table extends View {
    private Map<String, Column> columnMap;
    private Map<String, Set<Column>> referentialIntegrity;

    public Table(String name, Column[] columns) throws DuplicatedColumnException {
        super(name, columns);

        this.columnMap = new HashMap<>();
        this.referentialIntegrity = new HashMap<>();
        for (Column c : columns) {
            try { this.addColumn(c); } catch (SQLException e) {}
        }
    }

    @Override
    protected void setDriver(Driver driver) {
        this.driver = driver;
        for (Column c : columnMap.values()) {
            c.setDriver(driver);
        }
    }

    @Override
    public Column getColumn(String colName) {
        return columnMap.get(colName);
    }

    @Override
    public Column[] getColumns() {
        Collection<Column> cols = columnMap.values();
        return cols.toArray(new Column[cols.size()]);
    }

    public void addColumn(Column col) throws DuplicatedColumnException, SQLException {
        if (columnMap.containsKey(col.getName())) {
            throw new DuplicatedColumnException(col.getName());
        }
        if (col.getTable() != null) {
            try {
                col = new Column(col);
            } catch (InvalidTypeException e) {
                // Ignore
            } catch (ReferentialIntegrityException e) {
                // Ignore
            }
        }
        if (driver != null) {
            driver.addColumn(this, col);
        }
        columnMap.put(col.getName(), col);
        col.setTable(this);
        col.setDriver(driver);
    }

    public void dropColumn(String colName) throws ReferentialIntegrityException, ColumnNotFoundException, SQLException {
        Set<Column> refs = referentialIntegrity.get(colName);
        if (refs != null && !refs.isEmpty()) {
            throw new ReferentialIntegrityException(colName);
        }
        Column col = columnMap.get(colName);
        if (col == null) {
            throw new ColumnNotFoundException(colName);
        }
        if (driver != null) {
            driver.dropColumn(col);
        }
        columnMap.remove(colName);
        referentialIntegrity.remove(colName);
        Column ref = col.getForeignKeyReference();
        if (ref != null) {
            ref.getTable().dropReferentialConstraint(ref, col);
        }
        col.setTable(null);
        col.setDriver(null);
    }

    public void setPrimaryKey(Column[] columns)
            throws SQLException, InsufficientColumnsException, ReferentialIntegrityException, ColumnNotFoundException {
        if (columns.length == 0)
            throw new InsufficientColumnsException();

        for (Column col : columns) {
            if (col.getTable() != this)
                throw new ColumnNotFoundException();
            Set<Column> refs = referentialIntegrity.get(col.getName());
            if (refs != null && !refs.isEmpty())
                throw new ReferentialIntegrityException(col.getName());
        }

        if (driver != null) {
            driver.setTablePrimaryKey(this, columns);
        }
        for (Column col : columnMap.values())
            col.setPrimaryKey(false);
        for (Column col : columns)
            col.setPrimaryKey(true);
    }

    protected void renameColumn(Column col, String newName) throws DuplicatedColumnException, ColumnNotFoundException {
        if (columnMap.containsKey(newName)) {
            throw new DuplicatedColumnException(newName);
        }
        Column c = columnMap.remove(col.getName());
        if (!c.equals(col)) {
            throw new ColumnNotFoundException(col.getName());
        }
        columnMap.put(newName, col);

        Set<Column> refs = referentialIntegrity.get(col.getName());
        if (refs != null) {
            referentialIntegrity.put(newName, refs);
        }
    }

    protected boolean isReferenced(Column col) {
        Set<Column> refs = referentialIntegrity.get(col.getName());
        return refs != null && !refs.isEmpty();
    }

    protected void addReferentialConstraint(Column col, Column fk) {
        Set<Column> refs = referentialIntegrity.get(col.getName());
        if (refs == null) {
            refs = new HashSet<>();
            referentialIntegrity.put(col.getName(), refs);
        }
        refs.add(fk);
    }

    protected void dropReferentialConstraint(Column col, Column fk) {
        Set<Column> refs = referentialIntegrity.get(col.getName());
        if (refs != null)
            refs.remove(fk);
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Table)) {
            return false;
        }
        Table t = (Table) o;
        if (database == null || t.database == null || database != t.database) {
            return false;
        }
        return name.equals(t.name);
    }
}
