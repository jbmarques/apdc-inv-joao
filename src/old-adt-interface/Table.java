public interface Table {
    // Returns the name of this table.
    String getName();

    // Renames this table to `newName`.
    void rename(String newName);

    // Returns the attribute with the name `attributeName` from this table.
    Attribute getAttribute(String attributeName);

    // Returns this table attributes.
    Iterator<Attribute> getAttributes();

    // Adds an attribute to the table.
    void addAttribute(Attribute attribute);

    // Adds an attribute and populate the existing table entries with the given values.
    void addAttribute(Attribute attribute, Object[] values);

    // Removes an attribute from the table.
    void removeAttribute(Attribute attribute);

    // Insert values into all the table attributes.
    void insert(Object[] values);

    // Insert values into only the specified `attributes`.
    void insert(Set<Attribute> attributes, Object[][] values);

    Iterator<TableEntry> getEntries();
}
