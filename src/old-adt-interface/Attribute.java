public interface Attribute {
    // Returns the name of this Attribute.
    String getName();

    // Renames this attribute to `newName`.
    void rename(String newName);

    // Returns the attribute type.
    Class getType();

    // Changes the type of the attribute to `newType`.
    void setType(Class newType);

    // Returns the default value of this attribute.
    Object getDefaultValue();

    // Changes the default value of this attribute to `newDefaultValue`.
    void setDefaultValue(Object newDefaultValue);

    // Returns the constraints set for this attribute.
    Iterator<Constraint> getConstraints();

    // Add the constraint `c` to this attribute.
    void addConstraint(Constraint c);

    // Removes the constraint `c` from this attribute.
    void removeConstraint(Constraint c);
}
