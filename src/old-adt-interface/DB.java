public interface DB {
    // Returns the table with the given name, or null if the table does not exist.
    Table getTable(String name);

    // Adds the table `t` to the database.
    void addTable(Table t);

    // Removes the table `t` from the database.
    void removeTable(Table t);

    // Splits the table `t` into two tables. The first table contains the
    // attributes in `a1` and the second table contains the attributes in `a2`.
    // `a1` and `a2` must be disjoint and their attributes must belong to `t`.
    Table[] splitTable(Table t, Set<Attribute> a1, Set<Attribute> a2);

    // Joins the tables in `ts` into one table.
    Table joinTables(Set<Table> ts);

    // Closes the connection with the database.
    void close();
}
