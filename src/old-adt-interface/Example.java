public class Example {
    public static void main(String[] args) {
        DB db = new DBClass("postgresql://localhost:5740/accounting");

        Table users = db.getTable("users");

        // Print old user attributes.
        users.getAttributes().forEachRemaining(System.out::println);

        // New attribute with name 'city', type String and not null.
        Attribute city = new AttributeClass("city", String.class, Constraint.NOT_NULL);

        // Adding a not null attribute would make the table drop the existing data
        // or result in an error, so give values to populate the existing entries?
        String[] cities = {
                "Lisbon", "Madrid", "London", "London",
                "Berlin", "Dublin", "Paris", "Beijing",
        };
        users.addAttribute(city, cities);

        // Insert new users.
        String[][] newUsersData = {
                {"newUsername1", "some_email@gmail.com"    , "San Francisco"},
                {"newUsername2", "username2@yahoo.com"     , "Johannesburg"},
                {"Bill Gates"  , "bill_gates@microsoft.com", "Medina"},
        };
        users.insert(newUsersData);

        // Add suffix to all usernames.
        Attribute username = users.getAttribute("username");
        users.getEntries().forEachRemaining(entry -> {
            String old = (String)entry.get(username);
            entry.set(username, old + "_SOME_SUFFIX");
        });

        db.close();
    }
}
