public interface TableEntry {
    // Returns the value of this entry attribute `attribute`.
    Object get(Attribute attribute);

    // Sets this entry attribute `attribute` to the given value.
    void set(Attribute attribute, Object value);
}
