import static java.sql.Types.*;

public class Attribute {
    private String name;
    private String typeString;

    private int type;
    private int numChars;

    private boolean primaryKey;

    private String fkTable;
    private Attribute fkAttribute;

    private boolean nullable;
    private String defaultValue;

    public Attribute(String name, int type) {
        this.name = name;
        this.type = type;
        this.numChars = 0;

        this.primaryKey = false;

        this.fkTable = null;
        this.fkAttribute = null;

        this.nullable = true;
        this.typeString = SQLTypeToString(type);
    }

    public String getName() {
        return name;
    }

    public int getType() {
        return type;
    }

    public String getTypeString() {
        return typeString;
    }

    public void setNumChars(int numChars) {
        this.numChars = numChars;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public void setForeignKeyReference(String table, Attribute pk) {
        this.fkTable = table;
        this.fkAttribute = pk;
    }

    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("%s : %s", name, typeString));

        if (type == CHAR || type == VARCHAR || type == NCHAR || type == NVARCHAR)
            sb.append(String.format("(%d)", numChars));

        if (primaryKey)
            sb.append(" PRIMARY KEY");
        else if (fkTable != null && fkAttribute != null)
            sb.append(String.format(" FOREIGN KEY REFERENCES %s(%s)", fkTable, fkAttribute.name));
        else if (!nullable)
            sb.append(" NOT NULL");

        if (defaultValue != null)
            sb.append(String.format(" DEFAULT(%s)", defaultValue));

        return sb.toString();
    }

    public static String SQLTypeToString(int type) {
        switch (type) {
        case BIT       : return "BIT";
        case BOOLEAN   : return "BOOLEAN";
        case DOUBLE    : return "DOUBLE";
        case FLOAT     : return "FLOAT";
        case NUMERIC   : return "NUMERIC";
        case TINYINT   : return "TINYINT";
        case SMALLINT  : return "SMALLINT";
        case INTEGER   : return "INT";
        case BIGINT    : return "BIGINT";
        case CHAR      : return "CHAR";
        case VARCHAR   : return "VARCHAR";
        case NCHAR     : return "NCHAR";
        case NVARCHAR  : return "NVARCHAR";
        case TIMESTAMP : return "TIMESTAMP";
        default:
            return String.format("TYPE[%d]", type);
        }
    }
}
