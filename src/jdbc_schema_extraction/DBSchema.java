/**
 * Small utility program that gets the schema (or at least some parts of it) of
 * a database. A JDBC driver for the database system is required.
 *
 * The purpose of this tool was to better understand the schema extraction
 * methods offered by JDBC.
 */

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.Map;
import java.util.Properties;

import java.sql.*;

public class DBSchema {
    private static final int MAP_INITIAL_CAPACITY = 200;

    private static Map<String, Map<String, Attribute>> tables;

    public static void main(String[] args) {
        int argv = 0;
        String user = null;
        String pass = null;

        try {
            for (argv = 0; argv < args.length; argv++) {
                final String cmd = args[argv];

                if (cmd.charAt(0) != '-')
                    break;

                if (cmd.equals("-u")) {
                    user = args[++argv];
                } else if (cmd.equals("-p")) {
                    pass = args[++argv];
                } else {
                    System.err.printf("error: unrecognized command line option '%s'\n", cmd);
                    System.exit(1);
                }
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            // Missing command line argument.
            usage(1);
        }

        // Missing database url from command line options.
        if (argv == args.length)
            usage(1);

        try {
            getSchema(args[argv], user, pass);
            printSchema();
        } catch (SQLException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void usage(int status) {
        System.err.println("usage: java DBSchema [-u <user>] [-p <password>] <url>");
        System.exit(status);
    }

    private static void getSchema(String url, String user, String pass) throws SQLException {
        Properties props = new Properties();
        if (user != null) props.setProperty("user", user);
        if (pass != null) props.setProperty("password", pass);

        Connection conn = DriverManager.getConnection(url, props);

        tables = new HashMap<>(MAP_INITIAL_CAPACITY);
        DatabaseMetaData md = conn.getMetaData();

        // List only user defined tables.
        String[] types = { "TABLE" };

        ResultSet rs = md.getTables(null, null, "%", types);
        while (rs.next()) {
            String table = rs.getString(3);
            tables.put(table, getTableAttributes(md, table));
        }
        populateForeignKeys(md);
    }

    private static Map<String, Attribute> getTableAttributes(DatabaseMetaData md, String table) throws SQLException {
        Map<String, Attribute> attributes = new HashMap<String, Attribute>(MAP_INITIAL_CAPACITY);

        ResultSet rs = md.getColumns(null, null, table, "%");
        while (rs.next()) {
            String name = rs.getString(4);
            Attribute attr = new Attribute(name, rs.getInt(5));

            attr.setNumChars(rs.getInt(16));
            attr.setDefaultValue(rs.getString(13));

            if (rs.getString(18).equals("NO"))
                attr.setNullable(false);

            attributes.put(name, attr);
        }
        rs.close();

        rs = md.getPrimaryKeys(null, null, table);
        while (rs.next()) {
            Attribute attr = attributes.get(rs.getString(4));
            if (attr != null)
                attr.setPrimaryKey(true);
        }
        rs.close();

        return attributes;
    }

    private static void populateForeignKeys(DatabaseMetaData md) throws SQLException {
        for (String table : tables.keySet()) {
            Map<String, Attribute> attributes = tables.get(table);
            ResultSet rs = md.getImportedKeys(null, null, table);

            while (rs.next()) {
                String pkTable = rs.getString(3);
                Attribute pkAttribute = tables.get(pkTable).get(rs.getString(4));

                Attribute attr = attributes.get(rs.getString(8));
                attr.setForeignKeyReference(pkTable, pkAttribute);
            }
        }
    }

    private static void printSchema() {
        for (Map.Entry<String, Map<String, Attribute>> e : tables.entrySet()) {
            String table = e.getKey();
            Map<String, Attribute> attributes = e.getValue();

            System.out.printf("%s:\n", table);
            for (Attribute attribute : attributes.values())
                System.out.printf("\t%s\n", attribute);

            System.out.println("---------------------------------------------");
        }
    }
}
