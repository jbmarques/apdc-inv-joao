\documentclass{llncs}
\pagestyle{plain}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{microtype}
\usepackage{graphicx}
\usepackage{minted}

\usepackage[ colorlinks=true
           , linkcolor=black
           , urlcolor=black
           , citecolor=black
           , anchorcolor=black
           , pdfpagelabels
           , pdfdisplaydoctitle
           ]{hyperref}

\usepackage[ style=numeric
           , sorting=nyt
           , sortcites=true
           , hyperref=true
           , maxnames=5
           , giveninits
           , urldate=edtf
           , date=edtf
           , seconds=true
           ]{biblatex}
\addbibresource{report.bib}

\title{Interactive and Live Safe Manipulation of Database Schemas and Queries}
\author{João Marques \and João Costa Seco}
\institute{
    NOVA LINCS \& DI, FCT, Universidade NOVA de Lisboa\\
    \email{jb.marques@campus.fct.unl.pt}\\\email{joao.seco@fct.unl.pt}
}

\begin{document}
\maketitle

\begin{abstract}
Applications are subject to constant evolution with the frequent addition of new
features, the need to fix bugs, new security requirements and integration with
other systems. Databases are a critical part of such applications and dealing
with a changing database structure is a difficult maintenance problem. Schema
evolution deals exactly with this problem by offering ways to modify a database
schema without losing existing data. In this report we briefly discuss support
for schema evolution in popular database systems and database tools and present
a prototype of an abstract data type (ADT) to define, modify and evolve a
relational database of small to medium complexity that one usually finds in
small web applications. The main goal is to allow a user to define and modify a
database while maintaining some selected invariants such as referential
integrity without loss of existing data.

\keywords{Schema evolution \and DB \and Database \and Evolving database \and
Abstract data type.}
\end{abstract}

\section{Introduction}

Software evolution is an important process in the lifetime of any application,
there is a need to remain competitive and software providers are required to
frequently release new features, fix bugs and integrate with other systems and
applications. At the same time, data management is a vital task of such
applications and with the constant evolution process and the growing complexity
of the system as it ages, the maintenance of a database is more often than not
a difficult and troublesome task~\cite{roddick1995,curino2008}.

Schema evolution has arisen in response to this problem, as defined by
Roddick~\cite{roddick1995}, schema evolution is accommodated when a database
system facilitates the modification of the database schema without loss of
existing data. A stronger counterpart of schema evolution is schema versioning,
which is accommodated when a database system allows the accessing of all data,
both retrospectively and prospectively, through user definable version
interfaces. However, schema evolution and schema versioning is still not very
well accommodated by database systems~\cite{brahmia2015schema}, and most
proposed solutions appear as a form of third-party software~\cite{wueb2011}.

Supporting schema evolution is not without its problems, one needs to think
about the impact of schema modifications on existing data, queries and software
that make use of the database system. Schema modifications can render
applications in a non functional state, what works with an old schema might not
work with a newer schema. In addition, erroneous changes can happen and allowing
for their removal implies that operations should be as reversible as possible.
Manual updates to applications are required, queries are often embedded as
simple strings in application code and the compiler is not able to detect errors
resulting from a database structure change. Access to the database might also be
restricted while schema changes occur, in a multi-user environment it may be
possible to modify the database schema while another user is currently accessing
the database~\cite{roddick1995}. These situations can result in prolonged
downtime for other database users since they might have to wait for schema
modification operations to finish executing. In this report we tackle the
problem of database schema and queries manipulation in a safe manner by
providing an environment to make such manipulations without allowing operations
that might result in loss of data.

This report is structured as follows. In section~\ref{sec:schemaEvo} we briefly
discuss support for schema evolution in mainstream database systems,
specifically MySQL, PostgreSQL and Oracle, and third-party software that is
built on top of these databases and facilitate schema changes in a safe manner
without loss of existing data. In section~\ref{sec:schemaEvoStudy} we look at
long-term schema evolution studies to better understand how schema changes as
applications evolve. In section~\ref{sec:adt} we define an abstract data type
(ADT) that implements a reflection interface for a relational database that
serves as a foundation for a safe environment to define, modify and evolve a
relational database of small to medium complexity. By a safe environment we mean
evaluate if operations can be executed without violating defined table and
column constraints and without loss of existing data, except when explicitly
asked, such as \textit{drop table} or \textit{drop column} operations. Finally,
in section~\ref{sec:impl} we present a prototype implementation of the defined
ADT in Java using the JDBC API on top of PostgreSQL.

\section{Schema evolution in mainstream database systems} \label{sec:schemaEvo}

Existing mainstream database systems provide a limited support for schema
evolution, schema modifications are done through schema manipulation
languages~\footnote{The term data definition language (DDL) is also used to
refer to any formal language for describing data structures.} such as SQL,
usually with slightly different syntax depending on the host database system.
This results in database users and administrators using ad hoc techniques to
manage the evolution of a database schema and data~\cite{brahmia2015schema}.
Although limited, support for schema evolution is growing in some database
systems with the recent introduction of more advanced features, for example
transactional DDL in both PostgreSQL and Oracle.

\subsection{MySQL, PostgreSQL, Oracle}

Similar to most database systems, schema modification in MySQL, PostgreSQL and
Oracle is supported through the \texttt{CREATE TABLE}, \texttt{ALTER TABLE} and
\texttt{DROP TABLE} statements~\cite{postgreSQLSchemas,mysqlSchemaOps}.

Schema modification operations can happen in-place or in a temporary copy.
In-place operations are generally fast while operations that perform the
modifications in a temporary copy of the table can require a long time,
especially in large tables. Only simple operations are made in-place, such as
table renames, changes to the table metadata, like renaming columns, renaming
indexes and changing default values.

Operations made in a temporary copy result in limited concurrent access. While
the operation is on-going the original table is readable. Write operations that
begin after the schema modification are stalled until the new table is ready.
Reads will also be blocked at the point where the database system is ready to
install the new version of the table, at this point an exclusive lock must be
acquired.

Some schema modification operations can result in the alteration or loss of
existing data. One example is data type changes in MySQL which tries to convert
existing values to the new type as best as possible~\cite{mysqlAlterTable}. Say
you reduce a column's length, if any of the existing data does not fit in the
new length, that data will be truncated. It is possible to prevent such
operations from succeeding by enabling strict SQL mode. Oracle is better in this
regard as it allows modification of existing column definitions as long as this
operation does not result in changes to existing data~\cite{oracleAlterTable}.
PostgreSQL default behaviour is to cast old data to the new data type and give
an error in case of data loss. It also allows to specify how the new column
value is calculated from the old through the optional USING clause~
\cite{postgresAlterTable}. Here is an example from the documentation~
\cite{postgresAlterTable}, where we change an \texttt{integer} column containing
unix timestamps to \texttt{timestamp with time zone} via a \texttt{USING}
clause:

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{sql}
ALTER TABLE foo
    ALTER COLUMN unix_timestamp SET DATA TYPE timestamp with time zone
    USING
        timestamp with time zone 'epoch' + unix_timestamp * interval '1 second';
\end{minted}

Transactional DDL is available in both PostgreSQL and
Oracle~\cite{transactionalDDLPostgreSQL}, making it possible to bundle DDL
statements into transactions and rollback in case anything goes wrong, it
supports even large changes such as creating and dropping tables. In MySQL such
operations cannot be reversed~\cite{mysqlNoReverse}.

\subsection{Ruby on Rails Migrations}

Ruby on Rails migrations are a feature of the Rails framework that allow you to
evolve your database schema over time~\cite{railsMigrations}. Migrations are
files that contain a specific set of instructions necessary to take the database
schema from an old version to a newer version. These files can be versioned and
will act as a history of how the database has changed. The database can be
recreated from any point in time to the most recent version. Migrations are
database independent, since instructions are written in a ruby DSL instead of
SQL.

Using migrations provide developers with some benefits, it is easier to work
with other developers, when a new feature that requires database modifications
is implemented, the migration file can keep everyone up to date. They are also
heavily integrated in the Rails project, making it easier to deploy, the
framework knows which was the last executed migration and from that infer which
migrations are needed to get the database schema to it's latest version, this
process is seamless and happens without the user intervention.

Rails migrations should only be used for schema modification and not for data
manipulation. Trying to modify data within migrations will generally speaking
result in broken migrations. There is no guarantee that all developers will run
all migrations in their environment, one might use \texttt{rake db:schema:load}
or \texttt{rake db:reset} to load the database schema which will simply load the
latest version of the schema and ignore the migrations. In a situation like this
all the data manipulation operations will be skipped.

Another available option for data manipulation is to directly use SQL statements
with the \texttt{execute} command. To note that this technique depends on the
database system in use and makes you lose the agnostic characteristic of Rails
migrations, it also does not guarantee that everyone will run the migration.

To quote Agile Web Development with Rails~\cite{agileRails} on this subject,
"...migrations aren't really meant to carry seed data. They're too temporal in
nature to do that reliably. Migrations are here to bring you from one version of
the schema to the next, not to create a fresh schema from scratch - we have the
\texttt{db/schema.rb} file for that.

So, as soon as you actually get going with a real application, people won't be
running your early migrations when they set up the application. They'll start
from whatever version is stored in \texttt{db/schema.rb} and ignore all those
previous migrations. This means that any data created by the migrations never
make it into the database, so you can't rely on it."

\subsection{Third-party software}

Currently, schema evolution seems to be best supported by using third-party
software in conjunction with the database system. PRISM~\cite{curino-vldb2008a}
is one example of such software that tries to address schema evolution by
providing a language of schema modification operators to express concisely
complex schema changes, tools that allow the database administrator to evaluate
the effects of such changes, optimized translation of old queries to work on the
new schema version, automatic data migration, and full documentation of
intervened changes as needed to support data provenance and database flash back~
\cite{curino-vldb2008a}.

Migra~\cite{migra} is another example of such third-party software, it works on
top of PostgreSQL and facilitates schema migrations. It works much like the unix
tool \texttt{diff} but for database schemas. Provided two databases with similar
but slightly different schemas, migra will compare the schemas of both databases
and generate a migration script from one database to another. Migra helps make a
database migration process safer by allowing one to first execute schema changes
in a copy of the database, review the results with regards to their effect on
existing data, and if everything is ok, run the migration script against the
original database. Contrary to PRISM, this tool doesn't work on queries, it will
however, correctly update SQL functions. If these functions are used by
applications that use the database they will continue to work correctly after
the migration.

\section{Schema evolution analysis} \label{sec:schemaEvoStudy}

The first step towards supporting dynamic and safe manipulation of database
schemas and queries is understanding how databases change as applications
evolve. In this section we will review prior long-term studies on popular
applications with regards to schema evolution.

Sjøberg~\cite{sjoberg1993} studied schema evolution on a health management
system over an 18 month period, Curino et al.~\cite{curino2008} studied schema
evolution in Wikipedia over 4 years and 7 months. Finally, Wu et
al.~\cite{wueb2011} studied schema evolution in four popular open source
programs, Firefox, Monotone, BiblioteQ and Vienna, spanning 18 cumulative years.

All the above studies structure their findings in a similar manner: (i) nature
of the changes, meaning what schema modifications are more frequent over the
lifetime of the study; (ii) frequency and time of the modifications relative to
the program lifetime, did modifications happen more in the early or later stages
of the program development.

Findings are similar across all studies, \texttt{CREATE TABLE}, \texttt{DROP
TABLE}, \texttt{ADD COLUMN} and \texttt{DROP COLUMN} are by far the most
frequent changes, constituting around 75\% of all schema modifications. The rest
of schema modifications include renames of tables and columns, column type
changes, primary and foreign key changes and merge and split operations on
tables. Curino et al.~\cite{curino2008} also studied the impact of schema
changes on the applications, more specifically how queries valid on an old
schema version, succeed or fail on newer schema versions. They observed that
only 22\% of the queries remain valid throughout the schema evolution, with the
end result of failure in client applications. Most of their queries simply
failed and client applications are unable to extract information from the
database.

\section{An abstract data type for a relational database} \label{sec:adt}

Below we present the design of an abstract data type of a relational database.
Based on the studies cited in section~\ref{sec:schemaEvoStudy} we can infer that
the most important operations are the creation and removal of tables and columns
since these make up the majority of schema modifications. Although much less
frequent, modifications such as renames, type and key changes and merge and
split operations, still make up a relevant percentage of all modifications in
a database, around 25\% as seen in section~\ref{sec:schemaEvoStudy}. All of the
mentioned modifications are defined in the ADT, making it well suited even for
large applications over long periods of development.

The main goal of this ADT is to provide a safe environment to manipulate the
schema of a database and the corresponding data, where operations do not violate
constraints defined by its schema or result in loss of existing data, except for
the obvious cases like dropping a table or column. The ADT should also
provide a reflection interface of the database, it should be possible to
extract representations of tables, columns and their various properties such as
names, data types and constraints. The only requisite for using the ADT is that
a database already exists in the host database system. However, this database
can be empty, its schema can be defined by operations provided by the ADT, as
well as populating the database with new data.

In the description of the operations below some examples are shown using the ADT
implementation described in section~\ref{sec:impl}.

\subsection{Operations on tables}

\subsubsection{Create table.}

Given a table name and a set of columns this operation creates a new and empty
table in the database, with a structure specified by the given set of columns.
The name of the table must be distinct from the name of any other table or view
that exists in the database. If a table or view with that name exists, this
operation fails without any effect on the database.

\subsubsection{Drop table or view.}

Given a table or view, drop table removes the table or view from the database.
The table or view must exist in the database and must not be referenced by a
view or a foreign key constraint of another table, otherwise this operation must
fail without modifying the database.

\subsubsection{Rename table or view.}

Changes the name of a table or view. The given table or view must exist in the
database and the new name must be distinct from the name of any other table or
view in the database. This operation might break client applications queries
that make use of this table since a table with the old name no longer exists.

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}
Database db = new Database(DB_URL);

// Get reference to table 'foo'.
Table table = db.getTable("foo");
table.rename("bar");

// Query with the old reference should work.
Row[] rows = table.selectAll();

// This will print 'bar'.
System.out.println(table.getName());
\end{minted}

\subsubsection{Modify the primary key constraint of a table.}

Given a table and a set of columns that belong to that table and uniquely
identify all table rows, this operation creates or recreates the primary key
constraint of the given table. The columns cannot contain NULL values.

This operation should not execute if any of the columns that make the current
primary key are referenced by foreign keys of another table. Dealing with this
situation requires too much guessing to be done without the user assistance. One
should first remove foreign key constraints, recreate the primary key
constraint, and finally recreate the necessary foreign key constraints.

If any of the above requirements is not met, the operation should not execute
and leave the table unchanged.

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}
Database db = new Database(DB_URL);

Column id = new Column("id", Types.INTEGER);
Column email = new Column("email", Types.VARCHAR, 100);
Column name = new Column("name", Types.VARCHAR, 100);

Table table = new Table("info", new Column[] { id, email, name });
table.setPrimaryKey(new Column[] { id, email });

// Create table where the columns id and email are the primary key.
db.createTable(table);

// Say that the column id is enough to uniquely identify all table rows,
// we can change the primary key of the table again.
table.setPrimaryKey(new Column[] { id });
\end{minted}

\subsubsection{Join tables.}

The join operation combines the columns of two existing tables in the database
into a set that should be saved as a table or view. The user must specify the
name of the resulting table or view, the two tables for joining and the join
predicate. The ANSI standard specifies five types of joins, inner, left outer,
right outer, full outer and cross, the user should at least be able to specify
which of these types of joins the operation must do. The name of the resulting
table or view must be distinct from the name of any other table or view in the
database.

Two variants of the join operation must exist: (i) the result is saved as a view
that references the two original tables, which are kept unchanged; (ii) the
result is saved as a table and two views that replicate the original two tables
are created, these two newly created views reference only the table resulting
from the join operation and the two original tables are removed. Although the
second variant removes the two original tables, no loss of data should occur
since all existing data is now in the resulting table. Simple views are usually
automatically updated by the database system and \texttt{INSERT},
\texttt{UPDATE} and \texttt{DELETE} statements can be used on the view in the
same way as on a regular table~\cite{psqlView}. This means that data
modifications done on the view will reflect on the original table, this is the
wanted and desired behaviour, however not guaranteed on all views or all
database systems, in PostgreSQL for example this only happens if the following
conditions are meet~\cite{psqlView}:

\begin{itemize}
    \item The view must have exactly one entry in its \texttt{FROM} list, which
    must be a table or another updatable view.

    \item The view definition must not contain \texttt{WITH}, \texttt{DISTINCT},
    \texttt{GROUP BY}, \texttt{HAVING}, \texttt{LIMIT}, or \texttt{OFFSET}
    clauses at the top level.

    \item The view definition must not contain set operations (\texttt{UNION},
    \texttt{INTERSECT} or \texttt{EXCEPT}) at the top level.

    \item The view's select list must not contain any aggregates, window
    functions or set-returning functions.
\end{itemize}

This conditions might result in unexpected behaviour since one can't
insert new data or update existing data from a view resulting from the join
operation. This is not an easy problem to solve and is out of scope of this
project, for future work it would be interesting to have a consistent behaviour
across all types of views in regards to data manipulation.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\columnwidth]{join.png}
    \caption{Join tables operation example.}
    \label{fig:join}
\end{figure}

\subsubsection{Split table.}

The split operation breaks an existing table into two tables or views. Given two
names for the resulting tables or views, say $r1$ and $r2$, a table $t$ and a
set of columns $S \subset C$, where $C$ is the set of columns of the table $t$,
the split operation results in $r1$ having the set of columns $C \setminus S$
and $r2$ having the set of columns $S$. Note that the primary keys of the table
$t$ exist in both the relation $r1$ and $r2$.

The name of the resulting tables or views must be distinct from the name of any
other table or view in the database and the given set of columns $S$ mentioned
above must be a proper subset of $C$.

Much like the join operation, two variants of the split operation must exist:
(i) the result is saved as two views that reference the original table, which is
kept and unchanged; (ii) the result is saved as two distinct tables and a view
that replicates the original table is created. On the second variant the newly
created view references only the resulting two tables, the original table is
removed. No loss of existing data should result from the split operation. Again,
similar to the join operation, if the resulting views of the split operation are
not simple, it might not be possible to use data manipulation operations in
those views.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\columnwidth]{split.png}
    \caption{Split table operation example.}
    \label{fig:split}
\end{figure}

\subsection{Operations on columns}

Column operations assume an existing table in the database. All of the below
operations except for \textit{add column} are done in an existing column of the
table.

\subsubsection{Add column.}

Given a column name, data type and possibly a storage size, depending on the
provided data type, this operation adds a column to a table in the database. The
column name must be different from the name of any other column in the table,
otherwise the operation must fail without modifying the table.

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}
Database db = new Database(DB_URL);

Column id = new Column("id", Types.INTEGER);
Column goals = new Column("goals", Types.INTEGER);

Table stats = new Table("stats", new Column[] { id, goals });
db.createTable(stats);

Column assists = new Column("assists", Types.INTEGER);
stats.addColumn(assists);
\end{minted}

\subsubsection{Drop column.}

Given an existing column of a table in the database, removes said column from
the table structure, any table constraints involving the column will be removed
as well. The removed column must not be referenced by a view or foreign key
constraint of another table, if it is this operation will not be executed and
the column not removed. If the table contains only one column, the column cannot
be dropped.

\subsubsection{Rename column.}

Changes the name of a column. The new column name must be different from the
name of any other column in the table, otherwise the operation is not executed.
Similar to \textit{rename table}, this operation might break client applications
queries that make use of this column since a column with the old name no longer
exists.

\subsubsection{Convert column data type.}

Given a column of a table, a data type and possibly a storage size, depending on
the provided data type, this operation converts the column to the new data type,
if and only if that conversion does not result in the loss of existing data from
the database. For example, if one changes the column data type from
\texttt{VARCHAR(20)} to \texttt{VARCHAR(10)}, values may have to be truncated.
Or the column type goes from \texttt{DOUBLE} to \texttt{FLOAT}, which may result
in the loss of data due to the loss of precision. In these cases the operation
should not be carried forward.

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}
Database db = new Database(DB_URL);

Column id = new Column("id", Types.INTEGER);
Column points = new Column("points", Types.BIGINT);

Table stats = new Table("stats", new Column[] { id, points });
db.createTable(stats);

// All good since this operation does not result in data loss, there
// is no data in the table yet.
points.convert(Types.INTEGER);

// Insert a big value in the points column.
stats.insert(new Column[] { id, points }, new Object[] { 1, 654321 });

// This would result in loss of information since the value 654321 does
// not fit in a SMALLINT column, operation should fail.
points.convert(Types.SMALLINT);

// This however is allowed because BIGINT has enough storage capacity
// to hold the value 654321.
points.convert(Types.BIGINT);
\end{minted}

\subsubsection{Modify unique constraint.}

Adds or removes a unique constraint of a given column. When adding the unique
constraint, the column values must all be distinct, otherwise this operation
should fail and have no effect on existing data.

\subsubsection{Modify nullable constraint.}

Adds or removes a nullable constraint of a given column. When adding the
\texttt{NOT NULL} constraint, the column cannot contain \texttt{NULL} values,
otherwise this operation should fail and have no effect on existing data.

\subsubsection{Modify check constraint.}

Adds or removes a check constraint of a given column. Receives a predicate
involving only the column given as a parameter, specifying a requirement for
every row inserted into the table. The operation should fail in case of an
invalid predicate that refers to a column belonging to another table.

\subsubsection{Modify foreign key constraint.}

Given two columns $c$ and $ref$, $c$ from the table we are modifying and $ref$
from another table, adds or removes a foreign key constraint to the column $c$
referencing the column $ref$. The $ref$ column does not have to be a primary key
of another table but it needs to at least have a \texttt{UNIQUE} constraint in
another table.

\begin{minted}[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}
Database db = new Database(DB_URL);

Column id = new Column("id", Types.INTEGER);
Column goals = new Column("goals", Types.INTEGER);

Table stats = new Table("stats", new Column[] { id, goals });
stats.setPrimaryKey(new Column[] { id });

db.createTable(stats);

Column fk = new Column("fk_col", Types.INTEGER);

// References 'id' column from 'stats' table, all good.
fk.setForeignKeyReference(id);

// Not allowed, 'goals' column is not UNIQUE, the operation does not execute,
// 'fk_col' still references the 'stats.id' column.
try {
    fk.setForeignKeyReference(goals);
} catch (ReferentialIntegrityException e) {
    // Ignore, all good.
}

Column assists = new Column("assists", Types.INTEGER);
Table more_stats = new Table("more_stats", new Column[] { fk, assists });

db.createTable(more_stats);
\end{minted}

\subsection{Operations on data}

\subsubsection{Insert.}

Given an existing table or view, a set of columns that belong to said table or
view and the values associated to the given columns, this operation adds a new
row to the table. The types of the values provided must match the type of their
respective column, if they do not match, the operation must fail and provide the
user with an error. Trying to insert values outside the range of allowed values
should also result in an error and an operation that is not executed.

\subsubsection{Update.}

Given an existing table or view, a set of columns belonging to that table or
view, the values associated to the given columns and predicate, this operation
updates the values of the specified columns in all rows that satisfy the
predicate. Similar to \textit{insert}, the values provided must match the type
of their respective columns, also the columns present in the predicate must all
belong to the given table. In case any of the above requirements are not met,
the operation must fail without effect on the database.

\subsubsection{Select.}

Given an existing table or view, a set of columns belonging to that table or
view and a predicate. The \textit{select} operation retrieves rows from the
given table that satisfy the predicate, then the column values present in the
resulting rows are computed from the set of columns given as a parameter. The
columns present in the predicate must all belong to the given table.

\subsection{Reflection}

Apart from schema and data manipulation operations the ADT should also provide
reflection operations that allows a program that uses the ADT to examine the
database.

For tables or views is should be possible to obtain an object that represents an
existing table or view given its name as well as get all the tables or views
present in the database.

For columns, given a table and the name of an existing column it should be
possible to obtain an object representation of the column as well as its data
type, storage size depending on the data type of the column, and finally, its
contraints like foreign key or if that column is unique or nullable. Given a
table it should also be possible to obtain object representations of all
existing columns in the table.

\section{Implementation} \label{sec:impl}

In this section we briefly explain the implementation of a prototype of the
abstract data type defined in section~\ref{sec:adt}. The ADT was implemented in
Java using the JDBC API.

To use the ADT one first needs to create a Database object, providing an URL and
optionally a username and password, the JDBC API is then leveraged to establish
a connection to the database and extract the database schema. The
\texttt{DatabaseMetaData}~\cite{databaseMetadata} interface is used, it provides
information about the database existing tables and views, what is their
structure and table and column constraints such as primary keys, foreign keys,
unique, etc. Once this information is extracted it is kept in memory in order to
check the safety of the operations the user wishes to execute.

The various ADT operations are then organized in different classes, Database,
Table, View, Column and Row. Whenever a user calls a method, the abstract
database schema in memory is used to check for possible problems, duplicated
names, referential integrity, operation requisites not being met, etc. If it is
detected that the operation is not safe, for example it will result in loss of
existing data or violate referential integrity, an error is reported through the
use of exceptions. In the case that everything is ok then the operation is
translated to a set of SQL commands and executed using JDBC. As future work it
would be interesting to be able to combine a set of operations and evaluate if
they are safe or not without having to execute any of the operations.

When the connection is first established with the creation of the Database
object, two extra and optional parameters are available. The first one provides
the option to run every operation as a dry run, meaning that operations that
result in a modification of the database schema or data will be checked for
safety but will not be executed on the database system. The second parameter
allows for the possibility to log the generated SQL statements to an output
stream, such as standard output or a file. These two options in conjunction can
be used to create safe migration scripts for a running database, since one can
first check the safety of the operations without running anything against the
database system, and then once the safety is verified, grab the generated SQL
and run it.

This prototype uses only one database system, PostgreSQL was chosen because it
is relatively popular and open source. Since most database systems use slightly
different DDL syntax, the part that deals with generating the SQL statements is
abstracted behind an interface. This meets one of the goals of this project,
provide a proof of concept implementation that is passible of being extended. If
one wants to extend this work to use other database systems the only
requirements are a JDBC driver and an implementation of the mentioned interface.

An usage example of this implementation is available in
appendix~\ref{appendix:example}, accompanied by the resulting program output
and generated SQL statements.

The source code of the described ADT implementation is available in a git
repository at \url{https://bitbucket.org/jbmarques/apdc-inv-joao}.

\section{Conclusion}

In this report we proposed the definition of an ADT for an evolvable database.
Operations supported by the ADT were chosen based on prior schema evolution
studies. We also propose a prototype implementation of the ADT in Java. Note
that this implementation is not exhaustive as this was never the goal of this
project, both the ADT definition and implementation can be extended with more
complex operations through a composition of more trivial operations, we believe
that we provide here a good ground work for future research in this topic.

\printbibliography

\newpage
\appendix
\section{Example usage of the implemented ADT} \label{appendix:example}

Below is the source code of a program that uses the ADT implementation described
in section~\ref{sec:impl}. It's a simple example that shows how to create
tables, set primary key and foreign key constraints as well as column
constraints such as \texttt{NOT NULL}, insert, update and read rows from tables
and split and join tables. Some operations are meant to result in an error to
show how illegal operations are handled.

\inputminted[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{java}{Example.java}

The output of the above program is the following:

\inputminted[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{text}{output.txt}

Finally, the generated SQL resulting from running the above program is shown
below, note that some whitespace was added to improve readability.

\inputminted[autogobble, breaklines, frame=single, framesep=2mm, fontsize=\scriptsize, style=bw]{sql}{example.sql}

\end{document}
