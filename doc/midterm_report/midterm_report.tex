\documentclass[12pt,a4paper]{article}

\usepackage{parskip}
\usepackage{libertine}
\usepackage{libertinust1math}
\usepackage{inconsolata}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{microtype}

% Color tints, shades, tones and others.
\usepackage[dvipsnames]{xcolor}
\definecolor{MNBlue}{RGB}{0, 20, 115}

% Better looking captions.
\usepackage[ font=small
           , labelfont=bf
           ]{caption}

% Handle cross-referencing commands to produce hypertext links in the document.
\usepackage[ colorlinks=true
           , linkcolor=black
           , urlcolor=MNBlue
           , citecolor=MNBlue
           , anchorcolor=black
           , pdfpagelabels
           , pdfdisplaydoctitle
           ]{hyperref}

\usepackage[ style=numeric
           , sorting=nyt
           , sortcites=true
           , hyperref=true
           , maxnames=5
           , giveninits
           ]{biblatex}
\addbibresource{midterm_report.bib}

\title{Interactive and Live Safe Manipulation of Database Schemas and Queries}
\author{
  \normalsize\textit{author}\\João Marques
  \and
  \normalsize\textit{supervisor}\\João Costa Seco
}
\date{
  \small{
    Faculdade de Ciências e Tecnologia \\
    Universidade Nova de Lisboa        \\
    April, 2018
  }
}

\begin{document}

\maketitle

\section{Introduction}

The manipulation of a database schema can be, most of the times, a complex and
difficult process~\cite{roddick1995,curino2008}. Applications are subject to
constant evolution with the frequent addition of new features, new security
requirements and integration with other systems. The database is a critical part
of such applications and it is imperative that its evolution is graceful.

Schema evolution and schema versioning deal with this exact problem, the need to
retain existing data and software system functionality in the face of changing
database structure~\cite{roddick1995}. Supporting schema evolution in database
systems is not without its problems.

Modifications to a database schema must be propagated to the data and one might
employ a strict or lazy strategy, analogous to the concept of strict and lazy
evaluation in functional languages.

One must also consider the impact of schema evolution on existing software and
queries. Schema modifications can render applications in a non functional state,
what worked with an old schema might not work with a newer schema. In addition,
erroneous changes can happen and allowing for their removal implies that
operations should be as reversible as possible.

A comprehensive survey discussing modelling, architectural and query language
issues regarding relational schema evolution and schema versioning is done by
Roddick~\cite{roddick1995}.

This project focuses on the development of a proof of concept live and
interactive environment to define, modify and evolve a relational database of
small to medium complexity that one usually finds in small web applications. The
main goal is to allow a user to define and modify a database \emph{live} while
maintaining some selected invariants such as type coherence and referential
integrity.

The key objective is to provide a simple and solid base for future work rather
than develop a featureful system with no future.

\section{How to address the problem}

The plan is to define an abstract data type of an evolvable database,
specifically in terms of operations that preserve a set of invariants. This
should be implemented on top of an existing technology, such as \emph{MySQL} or
\emph{PostgreSQL}, and with extensibility in mind.

Finally we hope to build a graphical user interface running in a web browser
with a suitable interaction model. We do not intend to mimic current graphical
database editors but instead showcase an interactive interface to define and
evolve a database safely.

\section{Work Plan}

The work plan is presented below. In addition to the listed tasks, one meeting
with Professor João Costa Seco is held every week to review and discuss the work
done. Tasks already completed or that are currently being worked on are
accompanied by a brief explanation of what was done.

\vspace*{5mm}

\begin{tabular}{r|p{9.8cm}}
  \textit{\footnotesize{March 7 to March 16}} &
  \footnotesize\textbf{Study schema evolution and database migration} \\&
  \footnotesize{Understand what schema evolution is and what problems
  it tries to solve. What issues do database systems face when trying to
  support schema evolution.} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{March 16 to April 4}} &
  \footnotesize\textbf{Study schema evolution in mainstream technology} \\&
  \footnotesize{Study mainstream database systems, specifically \emph{MySQL},
  \emph{PostgreSQL} and \emph{Oracle}, to understand the level of support for
  schema evolution in current technologies. The \emph{Ruby on Rails} migrations
  feature was also looked into.} \\
\end{tabular}

\begin{tabular}{r|p{9.8cm}}
  \textit{\footnotesize{April 4 to mid-April}} &
  \footnotesize\textbf{List requirements and operations for an evolvable
  database} \\&
  \footnotesize{Based on the survey done in the previous weeks, gather a list
  of requirements and operations for an evolvable database. Currently looking
  into elementary operations, e.g. renaming or splitting a table.} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{late April}} &
  \footnotesize\textbf{Study related tools and methods} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{early May}} &
  \footnotesize\textbf{Study academic work and ideas} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{mid-May}} &
  \footnotesize\textbf{Define an ADT for an evolvable database} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{mid- to late May}} &
  \footnotesize\textbf{Connect the work done to a web GUI} \\
  \multicolumn{2}{c}{}\\

  \textit{\footnotesize{June}} &
  \footnotesize\textbf{Write the final report} \\
  \multicolumn{2}{c}{}\\
\end{tabular}

\printbibliography

\end{document}
