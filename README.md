# Interactive and Live Safe Manipulation of Database Schemas and Queries

A collection of notes, slides and source code for a research project with professor
João Costa Seco.

* The [doc](doc/) folder contains the reports delivered during the semester.

* The [src](src/) folder contains the source code of all the tools an programs
develop during the semester.

* The [slides](slides/) folder constains the slides of presentations done during the
semester.
